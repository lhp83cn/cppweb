## 功能说明
1. 作者初衷是编写一个web框架支持C++开发cgi程序，于是cppweb诞生了。
2. 作者希望cppweb是一个大一统的框架，即可用于传统服务端编程也可用于web编程，作者希望http协议能做的更多，框架包括以下两个核心服务：
```
webserver：业务服务容器，通过配置也可升级为服务注册中心与定时任务调度中心。
webrouter：接口路由网关服务，对外提供统一的流量入口，主要负责请求分发以及黑白名称配置。
```
3. cppweb在读数据采用epoll网络模型，以任务队列的方式处理具体请求，回包也在任务队列中处理，理论上cppweb可支持单机10000个以上的并发连接。
4. cppweb易拓展，作者开发Java、Python等模块，用于支持Java、Python等语言开发cgi程序，开发者可以直接使用C/C++、Java、Python等语言进行混合开发。
5. cppweb追求小而巧，对于开源库是拿来即用，源码工程自带zlib、sqlite等源码代码，开发者无需另外下载，再此感谢zlib、sqlite等开源库的作者与开发团队。
6. 基于cppweb的微服务集群框架如下图所示，图中<green>绿色</green>部分包括服务注册中心与业务服务集群由webserver服务构成；图中<red>红色</red>部分包括外部接口网关与内部接口网关由webrouter接口路由网关服务构成。关于cppweb的更多内容请可访问[https://www.winfengtech.com/cppweb](https://www.winfengtech.com/cppweb)查看。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0905/112334_5db2c0e0_392413.png)

## 测试数据
1. cppweb在普通PC机(4核8G)上至少可支持每秒10000笔请求。
2. cppweb在1核1G的低配centos系统上至少支持每秒3000笔请求。
3. 下图是cppweb自身的流量监控数据：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0905/112355_2fce47b9_392413.png)
## 安装编译
###### 下面我们以centos与ubuntu系统的安装编译为例，讲解如何编译运行cppweb开发框架。如果系统没有自带openssl开发客户端，需要自行进行安装，ubuntu下可以执行`apt-get install libssl-dev`命令进行安装。

1. 执行以下命令[下载源码](https://gitee.com/xungen/cppweb)，你需要使用root用户进行安装编译。
```
git clone https://gitee.com/xungen/cppweb.git
```

2. 如果是在windows环境下编译运行`cppweb`，你需要执行`git checkout windows`命令切换到`windows`分支，`windows`分支下已集成`mingw`编译环境，你可以在`git-bash`中进行编译安装。
<img alt='' style='max-width:100%;margin-top:5px' src='https://www.winfengtech.com/notefile?id=2022012217363508&dbid=data'/>

3. 进入源码目录先执行`chmod +x *.sh`命令，然后执行`./compile.sh`命令进行编译：
```
initialize configure
---------------------------------------------
1.check openssl success
2.check g++ compiler success
3.check java compiler success
4.create product directory success
---------------------------------------------
>>> initialize build-essential success

*** wait few minutes for compiling ***
```

4. 正常情况3~5分钟完成编译，编译成功后用`root`用户执行`./install.sh`进行安装，输出如下内容说明安装成功。
```
install cppweb
--------------------------------
1.packaging files
2.unpacking files
3.installing service
--------------------------------
>>> install cppweb success
```
5. 新建一个`cppweb`用户，登录`cppweb`用户执行以下命令将`/home/cppweb/webapp`目录初始化为的`cppweb`应用目录。
```
webserver -init /home/cppweb/webapp
```

6. 在`cppweb`用户下执行以下命令启动`webserver`服务。
```
nohup webserver /home/cppweb/webapp/etc/config.yml > /dev/null &
```

6. 执行`webserver -l`命令，可以查看`cppweb`服务日志，用浏览器打开`http://localhost:8888`地址进入`cppweb`管理中心，你可以用`root`与`system`账户登录`cppweb`管理中心，默认密码与用户名相同，首次登录后请马上修改登录密码。

## 配置文件`config.yml`示例

```yaml
#应用配置
app:
  #应用ID
  id: 1
  #监听端口
  port: 8888
  #应用名称
  name: cppweb
  #应用根目录
  path: /home/cppweb/webapp

  #安全链接配置
  ssl:
    #SSL监听端口
    port: 9999
    #证书文件路径
    certfile: /home/cppweb/webapp/etc/cert.crt
    #证书私钥文件路径
    prikeyfile: /home/cppweb/webapp/etc/cert.key

  #静态资源目录映射配置
  dir:
    res: res

  #静态资源文件映射配置
  url:
    favicon.ico: res/img/logo.png

  #注册中心服务地址
  route:
    port: 8888
    host: 127.0.0.1

  #应用开关配置
  switch:
    java: false
    route: true
    python: false

  connect:
    timeout: 60
    maxrequestsize: 10M
    maxrequestcount: 1000

#日志配置
log:
  path: /home/cppweb/webapp/log

#缓存配置
redis:
  #port: 6379
  #host: 127.0.0.1

#数据库配置
database:
  type: sqlite
  name: /home/cppweb/webapp/etc/sqlite.db
```