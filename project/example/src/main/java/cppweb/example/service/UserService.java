package cppweb.example.service;

import java.util.List;
import cppweb.example.dao.UserDao;
import cppweb.example.dao.po.UserPo;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class UserService {
	@Autowired
	UserDao userDao;

	public List<UserPo> getList(String user, String name) throws Exception{
		return userDao.getList(user, name);
	}
}