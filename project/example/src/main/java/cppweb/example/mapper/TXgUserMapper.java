package cppweb.example.mapper;

import cppweb.example.mapper.model.TXgUser;
import cppweb.example.mapper.model.TXgUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TXgUserMapper {
    long countByExample(TXgUserExample example);

    int deleteByExample(TXgUserExample example);

    int deleteByPrimaryKey(String user);

    int insert(TXgUser record);

    int insertSelective(TXgUser record);

    List<TXgUser> selectByExample(TXgUserExample example);

    TXgUser selectByPrimaryKey(String user);

    int updateByExampleSelective(@Param("record") TXgUser record, @Param("example") TXgUserExample example);

    int updateByExample(@Param("record") TXgUser record, @Param("example") TXgUserExample example);

    int updateByPrimaryKeySelective(TXgUser record);

    int updateByPrimaryKey(TXgUser record);
}