package cppweb.example.mapper.model;

import java.io.Serializable;

public class TXgUser implements Serializable {
    private String user;

    private String dbid;

    private String icon;

    private String name;

    private String password;

    private String menulist;

    private String grouplist;

    private String language;

    private Integer level;

    private Integer enabled;

    private String certno;

    private String mail;

    private String phone;

    private String address;

    private String remark;

    private String statetime;

    private static final long serialVersionUID = 1L;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDbid() {
        return dbid;
    }

    public void setDbid(String dbid) {
        this.dbid = dbid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMenulist() {
        return menulist;
    }

    public void setMenulist(String menulist) {
        this.menulist = menulist;
    }

    public String getGrouplist() {
        return grouplist;
    }

    public void setGrouplist(String grouplist) {
        this.grouplist = grouplist;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getCertno() {
        return certno;
    }

    public void setCertno(String certno) {
        this.certno = certno;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatetime() {
        return statetime;
    }

    public void setStatetime(String statetime) {
        this.statetime = statetime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", user=").append(user);
        sb.append(", dbid=").append(dbid);
        sb.append(", icon=").append(icon);
        sb.append(", name=").append(name);
        sb.append(", password=").append(password);
        sb.append(", menulist=").append(menulist);
        sb.append(", grouplist=").append(grouplist);
        sb.append(", language=").append(language);
        sb.append(", level=").append(level);
        sb.append(", enabled=").append(enabled);
        sb.append(", certno=").append(certno);
        sb.append(", mail=").append(mail);
        sb.append(", phone=").append(phone);
        sb.append(", address=").append(address);
        sb.append(", remark=").append(remark);
        sb.append(", statetime=").append(statetime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}