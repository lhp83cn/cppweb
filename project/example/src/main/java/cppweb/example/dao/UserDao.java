package cppweb.example.dao;

import stdx.Utils;
import java.util.List;
import java.util.ArrayList;
import webx.utils.DBConnect;
import cppweb.example.dao.po.UserPo;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
	public List<UserPo> getList(String user, String name) throws Exception{
		List<Object> params = new ArrayList<>();
		String sql = "SELECT " + Utils.GetFieldString(UserPo.class) + " FROM T_XG_USER WHERE 1=1";
		if (Utils.IsNotEmpty(user)){
			sql += " AND user LIKE ?";
			params.add("%" + user + "%");
		}
		if (Utils.IsNotEmpty(name)){
			sql += " AND name LIKE ?";
			params.add("%" + name + "%");
		}
		return DBConnect.SelectList(UserPo.class, sql, params.toArray());
	}
}