package cppweb.example.dao;

import java.util.List;
import cppweb.example.dao.po.UserPo;

public interface TestDao{
    List<UserPo> list(String user, String name);
}