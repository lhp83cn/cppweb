package cppweb.example.dao.po;

import stdx.Optional;
import stdx.Required;

public class UserPo {
	@Required("用户名")
	public String user;
	@Required("用户姓名")
	public String name;
	@Required("用户邮箱")
	public String mail;
	@Required("更新时间")
	public String statetime;
}