package cppweb.example;

import cppweb.spring.SpringWebApp;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application extends SpringWebApp{
	static{
		initialize(Application.class);
	}
}