package cppweb.example;

import webx.WebApp;
import webx.http.HttpRequest;
import webx.http.HttpResponse;

@WebApp.Path(value = "${package}/${filename}", access = "public")
public class Resource extends Application{
	static WebApp app = new webx.Resource(Resource.class);

	public void process(HttpRequest request, HttpResponse response) throws Exception{
		app.process(request, response);
	}
}