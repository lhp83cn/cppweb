package cppweb.example.app.mybatis;

import stdx.Optional;

public class Request {
	@Optional(value = "用户名（模糊匹配）", regex = "[a-zA-Z0-9_]{1,64}")
	public String user;
	@Optional(value = "用户姓名（模糊匹配）", length = {1, 64})
	public String name;
}