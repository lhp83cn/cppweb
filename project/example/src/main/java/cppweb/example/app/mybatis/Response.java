package cppweb.example.app.mybatis;

import cppweb.example.dao.po.UserPo;
import stdx.Optional;
import stdx.Required;

import java.util.List;

public class Response {
	@Required("错误码")
	public int code;
	@Required("错误描述")
	public String desc;
	@Optional("用户列表")
	public List<UserPo> list;
}