package cppweb.example.app.javatest;

import webx.WebApp;
import webx.LogFile;
import webx.json.JsonObject;
import webx.http.HttpRequest;
import webx.http.HttpResponse;
import cppweb.example.Application;
import cppweb.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

@WebApp.Path(value = "${package}", access = "public")
@WebApp.Document(request = Request.class, response = Response.class, remark = "查询用户信息")
public class Process extends Application{
	@Autowired
	UserService userService;

	public void process(Request request, Response response) throws Exception{
		LogFile.Trace("user[%s] name[%s]", request.user, request.name);
		response.list = userService.getList(request.user, request.name);
		response.code = response.list.size();
		response.desc = "success";
	}
	public void process(HttpRequest request, HttpResponse response) throws Exception{
		Response resdata = new Response();
		process(request.toObject(Request.class), resdata);
		response.setBody(JsonObject.FromObject(resdata).toString());
	}
}