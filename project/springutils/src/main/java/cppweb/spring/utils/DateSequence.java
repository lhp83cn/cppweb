package cppweb.spring.utils;

import stdx.Utils;
import java.util.concurrent.ThreadLocalRandom;

public class DateSequence{
    public static long get(){
		int idx = Math.abs(ThreadLocalRandom.current().nextInt());
        String val = Utils.GetDateTimeSequence() + String.format("%04d", idx % 10000);
        return Long.parseLong(val);
    }
    public static int getDate(long id){
    	return (int)(getDateTime(id) / 1000000L);
	}
	public static long getDateTime(long id){
		return id / 10000L;
	}
    public static String getDateTimeString(long id){
        id /= 10000L;
        long sec = id % 100L;
        long min = id / 100L % 100L;
        long hour = id / 10000L % 100L;
        long day = id / 1000000L % 100L;
        long mon = id / 100000000L % 100L;
        long year = id / 10000000000L % 10000L;
        return String.format("%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);
    }
}