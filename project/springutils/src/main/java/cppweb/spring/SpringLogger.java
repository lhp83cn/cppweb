package cppweb.spring;

import stdx.Utils;
import webx.WebApp;
import webx.LogFile;
import java.util.Hashtable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.LogConfigurationException;

public class SpringLogger implements org.apache.commons.logging.Log{
	static int started = 0;
	static SpringLogger logger = new SpringLogger();
	static SpringLogFactory factory = new SpringLogFactory();

	static class LogMap<K, V> extends Hashtable<K, V>{
		@Override
		public V get(Object key){
			return (V)factory;
		}
	};

	static class SpringLogFactory extends LogFactory{
		void setup(){
			factories = new LogMap<ClassLoader, LogFactory>();
		}

		@Override
		public void release(){
		}

		@Override
		public String[] getAttributeNames(){
			return null;
		}

		@Override
		public Object getAttribute(String name){
			return null;
		}

		@Override
		public void removeAttribute(String name){
		}

		@Override
		public void setAttribute(String name, Object value){
		}

		@Override
		public Log getInstance(Class clazz) throws LogConfigurationException {
			return logger;
		}

		@Override
		public Log getInstance(String name) throws LogConfigurationException{
			return logger;
		}
	}

	static void setup(){
		factory.setup();
		synchronized(factory){
			--started;
		}
	}

	static void finish(){
		synchronized(factory){
			++started;
		}
	}

	static boolean canUse(){
		if (started == 0){
			try {
				if (WebApp.GetStartStatus() > 0) started = 1;
			}
			catch (UnsatisfiedLinkError e){
			}
		}
		return started > 0;
	}

	@Override
	public boolean isErrorEnabled(){
		return true;
	}

	@Override
	public boolean isFatalEnabled(){
		return true;
	}

	@Override
	public boolean isWarnEnabled(){
		return true;
	}

	@Override
	public boolean isInfoEnabled(){
		return true;
	}

	@Override
	public boolean isTraceEnabled(){
		return true;
	}

	@Override
	public boolean isDebugEnabled(){
		return true;
	}

	@Override
	public void error(Object msg){
		LogFile.Error(String.valueOf(msg));
	}

	@Override
	public void error(Object msg, Throwable error){
		LogFile.Error(String.valueOf(msg));
		LogFile.Error(error);
	}

	@Override
	public void fatal(Object msg){
		LogFile.Error(String.valueOf(msg));
	}

	@Override
	public void fatal(Object msg, Throwable error){
		LogFile.Error(String.valueOf(msg));
		LogFile.Error(error);
	}

	@Override
	public void warn(Object msg){
		LogFile.Notice(String.valueOf(msg));
	}

	@Override
	public void warn(Object msg, Throwable error){
		LogFile.Notice(String.valueOf(msg));
		LogFile.Notice(error);
	}

	@Override
	public void info(Object msg){
		if (canUse()){
			LogFile.Trace(String.valueOf(msg));
		}
	}

	@Override
	public void info(Object msg, Throwable error){
		if (canUse()){
			LogFile.Trace(String.valueOf(msg));
			LogFile.Trace(error);
		}
	}

	@Override
	public void trace(Object msg){
		if (canUse()){
			LogFile.Debug(String.valueOf(msg));
		}
	}

	@Override
	public void trace(Object msg, Throwable error){
		if (canUse()){
			LogFile.Debug(String.valueOf(msg));
			LogFile.Debug(Utils.GetStackString(error));
		}
	}

	public void debug(Object msg){
		if (canUse()){
			LogFile.Debug(String.valueOf(msg));
		}
	}

	@Override
	public void debug(Object msg, Throwable error){
		if (canUse()){
			LogFile.Debug(String.valueOf(msg));
			LogFile.Debug(error);
		}
	}
}
