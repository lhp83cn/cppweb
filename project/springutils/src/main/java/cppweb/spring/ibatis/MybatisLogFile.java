package cppweb.spring.ibatis;

import webx.LogFile;

public class MybatisLogFile implements org.apache.ibatis.logging.Log {
    @Override
    public boolean isDebugEnabled(){
        return true;
    }

    @Override
    public boolean isTraceEnabled(){
        return true;
    }

    public MybatisLogFile(String name){
    }

    @Override
    public void warn(String msg){
        LogFile.Trace(LogFile.IMP, msg);
    }

    @Override
    public void debug(String msg){
        if (msg.startsWith("==>") || msg.startsWith("<==")){
            LogFile.Trace(LogFile.TIP, msg);
        }
        else{
            LogFile.Trace(LogFile.DBG, msg);
        }
    }

    @Override
    public void trace(String msg){
        if (msg.startsWith("<==")){
            LogFile.Trace(LogFile.DBG, msg);
        }
        else{
            LogFile.Trace(LogFile.INF, msg);
        }
    }

    @Override
    public void error(String msg){
        LogFile.Error(msg);
    }

    @Override
    public void error(String msg, Throwable error){
        LogFile.Error(msg);
        LogFile.Error(error);
    }
}
