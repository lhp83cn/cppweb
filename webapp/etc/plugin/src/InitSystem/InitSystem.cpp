#include <webx/route.h>

class InitSystem : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBCGI(CGI_PRIVATE, "@null")
DEFINE_HTTP_CGI_EXPORT_FUNC(InitSystem)

static HttpServer* app = HttpServer::Instance();

#ifdef XG_WITHPYTHON

#include <python/PythonLoader.h>

static PythonLoader python;

static BOOL PythonSetup(BOOL lock)
{
	bool inited = false;

	CHECK_FALSE_RETURN(python.canUse());

	if (lock)
	{
		PythonLocker lk;

		python.call("setup", ParamVector(app->getPath()), inited);
	}
	else
	{
		python.call("setup", ParamVector(app->getPath()), inited);
	}

	return inited;
}

#endif

EXTERN_DLL_FUNC void SetLogFlag(int flag)
{
	LogThread::Instance()->setLogFlag(flag);
}
EXTERN_DLL_FUNC int Loop(const char* path)
{
	const char* cmd[] = {"webserver"};

	if (path == NULL) return XG_PARAMERR;

	Process::Instance(ARR_LEN(cmd), (char**)(cmd));

	if (app->init(path)) app->loop();

	return XG_SYSERR;
}
EXTERN_DLL_FUNC void WriteLog(const char* tag, const char* msg)
{
	if (tag && msg)
	{
		int level = eINF;
		char flag = toupper(tag[0]);
		
		switch(flag)
		{
		case 'D':
			level = eDBG;
			break;
		case 'T':
			level = eTIP;
			break;
		case 'E':
			level = eERR;
			break;
		case 'I':
			if (toupper(tag[1]) == 'M') level = eIMP;
			break;
		}

		LogTrace(level, "%s", msg);
	}
}

EXTERN_DLL_FUNC int GetId()
{
	return app->getId();
}

EXTERN_DLL_FUNC int GetPort()
{
	return app->getPort();
}
EXTERN_DLL_FUNC const char* GetHost()
{
	return app->getHost().c_str();
}
EXTERN_DLL_FUNC const char* GetSequence()
{
	thread_local string val;

	val = app->getSequence();

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetMimeType(const char* key)
{
	thread_local string val;

	val = app->getMimeType(key);

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetParameter(const char* key)
{
	thread_local string val;

	val = webx::GetParameter(key);

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetRouteHost(const char* path)
{
	thread_local string val;
	HostItem item = webx::GetRouteHost(path);

	if (item.host.empty()) return NULL;

	val = item.toString();

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetConfig(const char* name, const char* key)
{
	thread_local string val;

	if (name == NULL) name = "";
	if (key == NULL) key = "";

	try
	{
		val = webx::GetConfig(name, key);
	}
	catch (Exception e)
	{
		LogTrace(eERR, "query confile[%s][%s] failed[%s]", name, key, e.getErrorString());
	}

	return val.c_str();
}
EXTERN_DLL_FUNC void SetCgiDefaultGroup(const char* path, const char* grouplist)
{
	if (path && grouplist) webx::SetCgiDefaultGroup(path, grouplist);
}
EXTERN_DLL_FUNC int DisableSession(const char* sid)
{
	try
	{
		sp<Session> session = webx::GetSession(sid);

		if (session)
		{
			session->disable();

			return XG_OK;
		}

		return XG_TIMEOUT;
	}
	catch (Exception e)
	{
	}

	return  XG_SYSERR;
}
EXTERN_DLL_FUNC int CreateSession(const char* sid, int timeout)
{
	try
	{
		sp<Session> session = webx::GetSession(sid, timeout);

		if (session) return XG_OK;
	}
	catch (Exception e)
	{
	}

	return XG_SYSERR;
}
EXTERN_DLL_FUNC const char* GetSession(const char* sid, const char* key)
{
	thread_local string val;

	try
	{
		sp<Session> session = webx::GetSession(sid);

		if (session)
		{
			val = session->get(key);
		}
		else
		{
			val.clear();
		}
	}
	catch (Exception e)
	{
		val.clear();
	}

	return val.c_str();
}
EXTERN_DLL_FUNC int SetSession(const char* sid, const char* key, const char* val)
{
	if (val == NULL) val = "";

	try
	{
		sp<Session> session = webx::GetSession(sid);

		if (!session) return XG_TIMEOUT;

		if (session->set(key, val)) return XG_OK;
	}
	catch (Exception e)
	{
	}

	return XG_SYSERR;
}
EXTERN_DLL_FUNC void SetCgiAccess(const char* path, const char* access)
{
	if (strcasecmp(access, "public") == 0)
	{
		app->setCgiAccess(path, CGI_PUBLIC);
	}
	else if (strcasecmp(access, "protect") == 0)
	{
		app->setCgiAccess(path, CGI_PROTECT);
	}
	else if (strcasecmp(access, "private") == 0)
	{
		app->setCgiAccess(path, CGI_PRIVATE);
	}
	else
	{
		app->setCgiAccess(path, CGI_DISABLE);
	}
}
EXTERN_DLL_FUNC void SetCgiExtdata(const char* path, const char* extdata)
{
	app->setCgiExtdata(path, extdata ? extdata : "");
}
EXTERN_DLL_FUNC int CheckAccess(const char* path, const char* param, const char* grouplist)
{
	typedef int (*CheckAccessFunc)(const char*, const char*, const char*);
	static CheckAccessFunc func = (CheckAccessFunc)Process::GetObject("HTTP_CHECK_ACCESS_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(path, param, grouplist);
}
EXTERN_DLL_FUNC int CheckLimit(const char* key, int maxtimes, int timeout)
{
	if (key == NULL || maxtimes <= 0 || timeout <= 0) return XG_PARAMERR;

	string name = stdx::format("%s:limit", key);

	if (RedisConnect::CanUse())
	{
		sp<RedisConnect> redis = RedisConnect::Instance();

		if (!redis) return XG_SYSERR;

		if (redis->incr(name, 1, timeout) > maxtimes) return XG_DAYLIMIT;
	}
	else
	{
		int times = 0;
		sp<Session> sess = app->getSession(name, timeout);

		if (!sess) return XG_SYSERR;

		sess->getInt("times", times);
		sess->setInt("times", ++times);

		if (times > maxtimes) return XG_DAYLIMIT;
	}

	return XG_OK;
}
EXTERN_DLL_FUNC void SetCgiDoc(const char* path, const char* reqdoc, const char* rspdoc, const char* remark)
{
	app->setCgiDoc(path, reqdoc, rspdoc, remark);
}
EXTERN_DLL_FUNC int GetLastRemoteStatus()
{
	return webx::GetLastRemoteStatus();
}
EXTERN_DLL_FUNC const char* GetRemoteResult(const char* path, const char* param, const char* contype, const char* cookie)
{
	thread_local SmartBuffer data;
	
	typedef SmartBuffer (*GetRemoteResultFunc)(const char*, const char*, const char*, const char*);
	static GetRemoteResultFunc func = (GetRemoteResultFunc)Process::GetObject("HTTP_GET_REMOTE_RESULT_FUNC");

	if (func == NULL)
	{
		data.free();
	}
	else
	{
		data = func(path, param, contype, cookie);
	}

	return data.str();
}

#ifdef XG_WITHJAVA

#include <java/JavaLoader.h>

#define cppstr(str) JavaLoader::CppString(env, str)
#define javastr(str) JavaLoader::JavaString(env, str)

static string jvmpath;
static JNIEnv* jvmenv;
static JavaLoader jvm;
static int jvmstarted = 0;
static string jvmclasspath;
static map<string, string> jvmcgimap;

static BOOL JavaSetup(BOOL lock)
{
	string msg;
	HttpDataNode node;

	CHECK_FALSE_RETURN(jvm.canUse());

	node.setValue("path", app->getPath());
	node.setValue("host", app->getHost());
	node.setValue("port", stdx::str(app->getPort()));

	msg = "\r\n\r\n" + node.toString();

	jvm.taskQueueCall("webx/Startup", "process", SmartBuffer(msg), true);

	return TRUE;
}

EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_GetPort(JNIEnv* env, jclass obj)
{
	return app->getPort();
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetId(JNIEnv* env, jclass obj)
{
	return javastr(app->getPath());
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetHost(JNIEnv* env, jclass obj)
{
	return javastr(app->getHost());
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetPath(JNIEnv* env, jclass obj)
{
	return javastr(app->getPath());
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetSequence(JNIEnv* env, jclass obj)
{
	return javastr(app->getSequence());
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_GetStartStatus(JNIEnv* env, jclass obj)
{
	return jvmstarted;
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_Loop(JNIEnv* env, jclass obj, jstring path)
{
	jvmenv = env;

	Loop(cppstr(path).c_str());
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_SetLogFlag(JNIEnv* env, jclass obj, jint flag)
{
	LogThread::Instance()->setLogFlag(flag);
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_SetClassPath(JNIEnv* env, jclass obj, jstring path)
{
	jvmclasspath = stdx::replace(cppstr(path), "\\", "/");
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetMimeType(JNIEnv* env, jclass obj, jstring key)
{
	return javastr(app->getMimeType(cppstr(key)));
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetParameter(JNIEnv* env, jclass obj, jstring key)
{
	return javastr(webx::GetParameter(cppstr(key)));
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetRouteHost(JNIEnv* env, jclass obj, jstring path)
{
	HostItem item = webx::GetRouteHost(cppstr(path));

	if (item.host.empty()) return NULL;

	return javastr(item.toString());
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_Trace(JNIEnv* env, jclass obj, jint level, jstring msg)
{
	LogTrace(level, cppstr(msg));
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetConfig(JNIEnv* env, jclass obj, jstring name, jstring key)
{
	return javastr(GetConfig(cppstr(name).c_str(), cppstr(key).c_str()));
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_SetCgiDefaultGroup(JNIEnv* env, jclass obj, jstring path, jstring grouplist)
{
	return SetCgiDefaultGroup(cppstr(path).c_str(), cppstr(grouplist).c_str());
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_DisableSession(JNIEnv* env, jclass obj, jstring sid)
{
	return DisableSession(cppstr(sid).c_str());
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetSession(JNIEnv* env, jclass obj, jstring sid, jstring key)
{
	return javastr(GetSession(cppstr(sid).c_str(), cppstr(key).c_str()));
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_CreateSession(JNIEnv* env, jclass obj, jstring sid, jint timeout)
{
	return CreateSession(cppstr(sid).c_str(), timeout);
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetWebAppPath(JNIEnv* env, jclass obj, jstring path, jstring filename)
{
	return javastr(HttpServer::GetWebAppPath(cppstr(path).c_str(), cppstr(filename).c_str()));
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_SetSession(JNIEnv* env, jclass obj, jstring sid, jstring key, jstring val)
{
	return SetSession(cppstr(sid).c_str(), cppstr(key).c_str(), cppstr(val).c_str());
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_SetCgiAccess(JNIEnv* env, jclass obj, jstring path, jstring access)
{
	SetCgiAccess(cppstr(path).c_str(), cppstr(access).c_str());
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_SetCgiExtdata(JNIEnv* env, jclass obj, jstring path, jstring extdata)
{
	SetCgiExtdata(cppstr(path).c_str(), cppstr(extdata).c_str());
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_CheckLimit(JNIEnv* env, jclass obj, jstring key, jint maxtimes, jint timeout)
{
	return CheckLimit(cppstr(key).c_str(), maxtimes, timeout);
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_CheckAccess(JNIEnv* env, jclass obj, jstring path, jstring param, jstring grouplist)
{
	return CheckAccess(cppstr(path).c_str(), cppstr(param).c_str(), cppstr(grouplist).c_str());
}
EXTERN_DLL_FUNC void JNICALL Java_webx_WebApp_SetCgiDoc(JNIEnv* env, jclass obj, jstring path, jstring reqdoc, jstring rspdoc, jstring remark)
{
	SetCgiDoc(cppstr(path).c_str(), cppstr(reqdoc).c_str(), cppstr(rspdoc).c_str(), cppstr(remark).c_str());
}
EXTERN_DLL_FUNC jint JNICALL Java_webx_WebApp_GetLastRemoteStatus(JNIEnv* env, jclass obj)
{
	return webx::GetLastRemoteStatus();
}
EXTERN_DLL_FUNC jstring JNICALL Java_webx_WebApp_GetRemoteResult(JNIEnv* env, jclass obj, jstring path, jstring param, jstring contype, jstring cookie)
{
	SmartBuffer data = webx::GetRemoteResult(cppstr(path), cppstr(param), cppstr(contype), cppstr(cookie));

	if (data.isNull()) return NULL;

	return javastr(data.str());
}

#endif

static void CoverConfigFile()
{
	if (RedisConnect::CanUse())
	{
		sp<RedisConnect> redis = RedisConnect::Instance();

		if (redis)
		{
			YAMLoader::Instance()->set("redis.password", redis->getPassword());
			YAMLoader::Instance()->set("redis.host", redis->getHost());
			YAMLoader::Instance()->set("redis.port", redis->getPort());
		}
	}
}
class ReloadWorkItem : public WorkItem
{
public:
	void run()
	{
		CoverConfigFile();

#ifdef XG_WITHJAVA
		JavaSetup(true);
#endif

#ifdef XG_WITHPYTHON
		PythonSetup(true);
#endif
	}
};

static void ReloadSystemConfig()
{
	stdx::async(newsp<ReloadWorkItem>());

	Sleep(100);
}

int InitSystem::process()
{
	static int inited = 0;

	if (inited <= 0)
	{
		auto loadPlugin = [](string path){
			CHECK_FALSE_RETURN(path::type(path) == ePATH);

			vector<string> vec;

			CHECK_FALSE_RETURN(stdx::FindFile(vec, path, "*.so") > 0);

			if (!stdx::endwith(path, "/")) path += "/";

			for (auto& item : vec) item = item.substr(path.length());

			std::sort(vec.begin(), vec.end(), [](const string& a, const string& b){
				if (a == "RouteModule.so") return false;
				if (b == "RouteModule.so") return true;
				return a < b;
			});

			for (auto& item : vec)
			{
				if (item == "InitSystem.so") continue;

				webx::LoadHttpPlugin(path + item);
			}

			return true;
		};

		string localplugpath = app->getPath() + "etc/plugin/bin";
		string globalplugpath = stdx::translate("$CPPWEB_INSTALL_HOME/webapp/etc/plugin/bin");

		if (localplugpath == globalplugpath)
		{
			loadPlugin(localplugpath);
		}
		else
		{
			loadPlugin(globalplugpath);
			loadPlugin(localplugpath);
		}

		Process::SetObject("HTTP_RELOAD_SYSTEM_CONFIG_FUNC", (void*)ReloadSystemConfig);

		string path = proc::env("CPPWEB_PRODUCT_HOME");

		if (path.length() > 0)
		{
			Process::RegisterLibraryPath(path + "/lib/openssl/lib");
			Process::RegisterLibraryPath(path + "/lib/mysql/lib");
		}

		CoverConfigFile();

		inited = XG_OK;
	}

	int res = 0;

#ifdef XG_LINUX
	string envspliter = ":";
#else
	string envspliter = ";";
#endif

#ifdef XG_WITHJAVA
	if (request && response)
	{
		if (jvm.canUse())
		{
			string key;
			string url = request->getPath();
			auto it = jvmcgimap.find(stdx::tolower(key = url));

			if (it != jvmcgimap.end())
			{
				string data = request->getDataString();
				string head = ":path: " + url + "\r\n" + request->getHeadString();
				SmartBuffer buffer(head.length() + data.length() + 4);

				memcpy(buffer.str(), head.c_str(), head.length());
				memcpy(buffer.str() + head.length(), "\r\n\r\n", 4);
				memcpy(buffer.str() + head.length() + 4, data.c_str(), data.length());

				buffer = jvm.taskQueueCall(it->second, "process", buffer, true, &res);

				if (buffer.isNull()) return XG_SYSERR;

				if (res < 0) return res;

				char* end = buffer.str() + buffer.size();
				char* str = strstr(buffer.str(), "\r\n\r\n");

				if (str)
				{
					int len = 0;
					HttpHeadNode hdr;

					if (hdr.parse(string(buffer.str(), str)))
					{
						const map<string, string>& datmap = hdr.getDataMap();
						
						for (auto& item : datmap)
						{
							const string& key = item.first;
							const string& val = item.second;

							if (key == ":status")
							{
								response->setStatus(stdx::atoi(val.c_str()));
							}
							else
							{
								response->setHeadValue(key, val);
							}
						}
					}

					str += 4;

					if ((len = end - str) <= 0) return XG_FAIL;

					memmove(buffer.str(), str, len);
					buffer.truncate(len);
				}

				return createFile(buffer.size()) ? file->write(buffer.str(), buffer.size()) : XG_SYSERR;
			}
		}
	}
	else
	{
		auto reload = [&](){
			string url;

			for (auto item : jvmcgimap) app->addCgiData(url = item.first);
			
			LogTrace(eINF, "load java cgimap success");
		};

		if (jvm.canUse())
		{
			reload();
		}
		else if (HttpServer::SkipLoadWebApp())
		{
			if (jvmclasspath.length() > 0 || yaml::config<bool>("app.switch.java"))
			{
				jvmpath = Process::GetEnv("CLASSPATH");

				if (jvmclasspath.length() > 0) jvmpath += envspliter + jvmclasspath;

				if (jvmenv ? jvm.init(jvmenv) : jvm.init(jvmpath)) JavaSetup(false);
			}
		}
		else
		{
			jvmstarted = XG_ERROR;

			if (jvmclasspath.length() > 0 || yaml::config<bool>("app.switch.java"))
			{
				string jarhome;
				string javahome;
				set<string> pathset;
				set<string> classvec;
				vector<string> pathvec;

				jvmpath = app->getPath() + "jar";

				if (jvmclasspath.length() > 0) jvmpath += envspliter + jvmclasspath;
				
				jvmpath = stdx::replace(jvmpath, "\\", "/");
				jvmpath = stdx::replace(jvmpath, "//", "/");
				jvmpath = stdx::trim(jvmpath, envspliter);
				stdx::split(pathvec, jvmpath, envspliter);

				javahome = Process::GetEnv("JAVA_HOME");
				javahome = stdx::replace(javahome, "\\", "/");
				javahome = stdx::replace(javahome, "//", "/");

				jarhome = stdx::translate("$CPPWEB_INSTALL_HOME/product/jar");
				jarhome = stdx::replace(jarhome, "\\", "/");
				jarhome = stdx::replace(jarhome, "//", "/");

				std::sort(pathvec.begin(), pathvec.end(), [](const string& a, const string& b){
					return a.length() > b.length();
				});
		
				for (size_t i = 0; i < pathvec.size(); i++)
				{
					size_t pos;
					vector<string> vec;
					string jarpath = pathvec[i];

					if (jarpath.find(jarhome) == 0 || jarpath.find(javahome) == 0) continue;

					if (path::type(jarpath) == ePATH)
					{
						if (stdx::FindFile(vec, jarpath, "*.jar") > 0)
						{
							for (string& item : vec)
							{
								jvmpath += envspliter + item;

								pathvec.push_back(item);
							}
						}

						vec.clear();

						if (stdx::FindFile(vec, jarpath, "*.class") > 0)
						{
							if (jarpath.back() == '/' || jarpath.back() == '\\')
							{
								pos = jarpath.length();
							}
							else
							{
								pos = jarpath.length() + 1;
							}

							for (string& item : vec)
							{
								if (pathset.insert(item).second)
								{
									classvec.insert(item.substr(pos));
								}
							}
						}
					}
					else
					{
						string name = path::name(jarpath);

						if (name.find("log") == 0 || name.find("spring") == 0) continue;

						SmartBuffer data = proc::exec("jar", "tvf", jarpath);

						if (data.isNull())
						{
							LogTrace(eERR, "analysis package[%s] failed", jarpath.c_str());
						}
						else
						{
							stdx::split(vec, data.str(), "\n");
							
							for (string& item : vec)
							{
								if ((pos = item.rfind(' ')) == string::npos) continue;
								
								item = item.substr(pos + 1);
								
								if (item.find(".class") == string::npos) continue;

								classvec.insert(item);
							}
						}
					}
				}

				if (classvec.size() > 0)
				{
					if (jvmenv)
					{
						jvm.init(jvmenv);
					}
					else
					{
						vector<string> vec;

						stdx::FindFile(vec, jarhome, "*.jar");

						std::sort(vec.begin(), vec.end(), [&](const string& a, const string& b){
							return a.length() < b.length();
						});

						for (auto& item : vec) jvmpath += envspliter + item;

						jvmpath += envspliter + Process::GetEnv("CLASSPATH");

						LogTrace(eINF, "java classpath[%s]", jvmpath.c_str());

						jvm.init(jvmpath);
					}

					if (JavaSetup(false))
					{
						string url;
						string key;
						SmartBuffer data;

						for (auto& item : classvec)
						{
							url = string(item.c_str(), item.c_str() + item.rfind('.'));

							data = jvm.taskQueueCall("webx/Startup", "getCgiPath", SmartBuffer(url), true, &res);

							if (data.isNull()) continue;

							key = HttpServer::GetWebAppPath(data.str(), url.c_str());

							if (key.empty()) continue;

							jvmcgimap[stdx::tolower(key)] = url;

							LogTrace(eINF, "load class[%s][%s] success", key.c_str(), url.c_str());
						}

						LogTrace(eINF, "initialize java module success");

						jvmstarted = XG_OK;

						reload();
					}
					else
					{
						LogTrace(eERR, "import java module failed");
					}
				}
			}
		}
	}
#endif

#ifdef XG_WITHPYTHON
	if (request && response)
	{
		if (python.canUse())
		{
			int val = 0;
			PythonLocker lk;
			PythonLoader rs;

			string url = request->getPath();
			string head = request->getHeadString();
			string data = request->getDataString();

			if (python.call("process", ParamVector(url, head, data), rs))
			{
				int sz = rs.size();
				SmartBuffer buffer;

				if (sz < 0)
				{
					if (rs.isNumber()) return rs.getInteger();

					buffer = rs.getBuffer();
				}
				else if (sz > 0)
				{
					if (rs.getItem(0, buffer))
					{
						if (buffer.isNull()) buffer = rs.getItem(0);

						if (sz > 1)
						{
							PythonLoader hdr;
							vector<string> vec;
							
							if (rs.getItem(1, hdr) && hdr.getKeys(vec) > 0)
							{
								for (const string& key : vec)
								{
									const string& val = hdr.getItem(key);

									if (key == ":status")
									{
										response->setStatus(stdx::atoi(val.c_str()));
									}
									else
									{
										response->setHeadValue(key, val);
									}
								}
							}
						}
					}
					else
					{
						return XG_SYSERR;
					}
				}

				if (buffer.size() > 0) return createFile(buffer.size()) ? file->write(buffer.str(), buffer.size()) : XG_SYSERR;
			}
		}
	}
	else
	{
		auto reload = [&](){
			PythonLoader rs;
			vector<string> vec;

			if (HttpServer::SkipLoadWebApp()) return;

			if (python.call("getcgimap", ParamVector(), rs) && rs.getKeys(vec) > 0)
			{
				for (auto& key : vec) app->addCgiData(key);

				LogTrace(eINF, "load python cgimap success");
			}
			else
			{
				LogTrace(eERR, "load python cgimap failed");
			}
		};

		if (python.canUse())
		{
			PythonLocker lk;

			reload();
		}
		else
		{
			if (yaml::config<bool>("app.switch.python"))
			{
#ifdef XG_LINUX
				string libpath;
				vector<string> vec;

				libpath = stdx::translate("$CPPWEB_INSTALL_HOME/product/lib/python/lib");

				if (stdx::FindFile(vec, libpath, "libpython*.so*") > 0) dlopen(vec[0].c_str(), RTLD_LAZY | RTLD_GLOBAL);
#endif
				PythonLoader::Setup(true);

				PythonLocker lock;

				if (python.import(app->getPath() + "pyc/__load__.py"))
				{
					if (PythonSetup(false))
					{
						LogTrace(eINF, "initialize python module success");

						reload();
					}
					else
					{
						LogTrace(eERR, "initialize python module failed");
					}
				}
				else
				{
					LogTrace(eERR, "import python module failed");
				}
			}
		}
	}
#endif

	return request && response ?  XG_NOTFOUND : XG_OK;
}