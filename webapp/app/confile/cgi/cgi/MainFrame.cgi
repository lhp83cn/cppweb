<%@ path=/confile/${filename}%>
<%
	int line = -1;

	param_string(path);
	param_string(icon);
	param_string(title);
	param_string(source);
	param_string(keyword);

	if (title.empty()) title = "配置文件";

	if (icon.empty()) icon = "/res/img/file/configure.png";

	if (source.empty() && path.length() > 0 && path.length() < 1024)
	{
		HttpRequest request;

		request.init(path);
		request.setHeadValue("Cookie", this->request->getHeadValue("Cookie"));

		sp<HttpResponse> response = app->getLocaleResult(request);

		if (response)
		{
			SmartBuffer buffer = response->getResult();
			
			if (buffer.size() > 0 && buffer.size() < 256 * 1024) source = buffer.str();
			
			const char* str = SkipStartString(source.c_str(), " \r\n\t");
			
			if (str && *str == '<') source.clear();
		}
	}

	if (source.length() > 256 * 1024) source.clear();
	
	if (source.length() > 0)
	{
		if (keyword.length() > 0)
		{
			size_t pos = source.find(keyword);
			
			if (pos != string::npos)
			{
				line = 0;

				for (size_t i = 0; i < pos; i++)
				{
					if (source[i] == '\n') ++line;
				}
			}
		}

		source = stdx::replace(source, "\\", "\\\\");
		source = stdx::replace(source, "\n", "\\n");
		source = stdx::replace(source, "\r", "\\r");
		source = stdx::replace(source, "\t", "\\t");
		source = stdx::replace(source, "\"", "\\\"");
		source = stdx::replace(source, "\'", "\\\'");
	}
%>
<!DOCTYPE HTML>
<html>
<html manifest='/res/etc/manifest.cache'>
<head>
<title><%=title%></title>
<meta name='referrer' content='always'/>
<link rel='shortcut icon' href='<%=icon%>'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'/>
<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>

<link rel='stylesheet' type='text/css' href='/app/workspace/css/base.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/codemirror/lib/codemirror.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/codemirror/addon/fold/foldgutter.css'/>

<script>
if (typeof(require) == 'function') delete window.module;
</script>

<script src='/res/lib/utils.js.gzip'></script>
<script src='/res/lib/codemirror/lib/codemirror.js.gzip'></script>

<script src='/res/lib/codemirror/mode/shell/shell.js'></script>
<script src='/res/lib/codemirror/addon/fold/foldcode.js'></script>
<script src='/res/lib/codemirror/addon/fold/foldgutter.js'></script>

<style>
#CodeDiv{
	height: 94vh;
	border: 1px solid #CCC;
}
.CodeMirror{
	background: rgba(255, 255, 255, 0.5);
}
.CodeMirror-gutters{
	background: rgba(255, 255, 255, 0.6);
}
</style>

<script>
$(document).ready(function(){
	var line = <%=line%>;
	var height = getClientHeight();

	getVue('ContentDiv');

	$('#CodeDiv').height(height - 2);

	var code = CodeMirror.fromTextArea(document.getElementById('CodeText'), {
		mode: 'shell',
		tabSize: 4,
		indentUnit: 4,
		inputStyle: 'textarea',
		foldGutter: true,
		lineNumbers: true,
		lineWrapping: true,
		matchBrackets: true,
		indentWithTabs: true,
		styleActiveLine: true,
		showCursorWhenSelecting: true,
		gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter']
	});

	code.setValue('\n\n\n\n\n\n\n\n\n\n');
	code.setValue('<%=source%>');
	code.setSize('100%','100%');

	if (line >= 0){
		setTimeout(function(){
			code.setSelection({line: line, ch: 0}, {line: line, ch: 1000}, {scroll: true});
		}, 500);
	}
});
</script>
</head>

<body>
<div id='ContentDiv'>
	<div id='CmdParamDiv'></div>
	<div id='CodeDiv'>
		<textarea id='CodeText'></textarea>
	</div>
</div>
</body>
</html>
