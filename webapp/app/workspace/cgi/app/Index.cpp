#include <webx/route.h>
#include <http/HttpHelper.h>

class Index : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(Index)

int Index::process()
{
	param_string(icon);
	param_string(code);
	param_string(title);
	param_string(session);

	if (icon.empty()) icon = "/favicon.ico";

	if (title.empty()) title = webx::GetParameter("TITLE");

	if (session.length() > 8 && session.length() < 1024)
	{
		char buffer[4096] = {0};

		HEXDecode(session.c_str(), session.length(), buffer);

		session = buffer;
	}

	string path;
	SmartBuffer buffer;

	if (request->isMobile())
	{
		path = "app/workspace/pub/mobile.htm";
	}
	else
	{
		path = "app/workspace/pub/index.htm";
	}

	CgiMapData cfg = app->getCgiMapData(path);

	if (cfg.url.empty() || app->getFileContent(cfg.url, buffer) <= 0) return XG_NOTFOUND;

	string msg = buffer.str();
	
	msg = stdx::replace(msg, "<%=icon%>", icon);
	msg = stdx::replace(msg, "<%=code%>", code);
	msg = stdx::replace(msg, "<%=title%>", title);
	msg = stdx::replace(msg, "<%=session%>", session);

	clearResponse();

	out << msg;

	return XG_OK;
}