<%@ path=${filename}%>
<%@ include="dbentity/T_XG_USER.h"%>
<%
	string user;

	try
	{
		user = checkLogin();
	}
	catch(Exception e)
	{
	}
%>

<table id='TitleTable' height='100%'>
	<tr>
		<td><label class='TitleLink' id='ShowLoginPageButton'><%=(user.empty() ? "登录" : "登出")%></label></td>
	</tr>
</table>

<script>
getVue('TitleTable');
setCurrentUser('<%=user%>');

function addTitleLink(text, click){
	let id = "TitleLink" + getSequence();
	$('#TitleTable tr').prepend("<td><label class='TitleLink' id='" + id + "'>" + text + "</label></td>");
	return $('#' + id).click(click);
}

addTitleLink('关于我们', function(){
	window.open('/sharenote?title=ABOUT&name=关于我们');
});

<%if (user.length() > 0){%>
addTitleLink('用户中心', function(){
	showToastMessage(getHttpResult('/app/workspace/pub/userinfo.htm'), true, null, null, true);
});
<%}%>

$('#ShowLoginPageButton').click(function(){
<%if (user.empty()){%>
	showLoginDialog(true);
<%}else{%>
	showConfirmMessage('&nbsp;&nbsp;是否要退出当前登录？&nbsp;&nbsp;', '退出登录', function(flag){
		if (flag){
			getHttpResult('/checklogin', {flag: 'Q'});
			clearCookie();
			updateTitle();
			updateMenu(true);
		}
	});
<%}%>
});
</script>