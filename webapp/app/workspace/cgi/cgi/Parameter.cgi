<%@ path=${filename}%>
<%@ include="dbentity/T_XG_PARAM.h"%>

<div><button v-show='parameteraccess>=0' @click='addParamRecord()' class='TextButton'><green>添加参数</green></div>

<%
	param_string(flag);

	if (flag.length() > 0)
	{
		param_string(id);
		param_string(name);
		param_string(param);
		param_string(filter);
		param_string(remark);

		webx::CheckFileName(id, 0);
		webx::CheckFileName(name, 0);

		checkLogin();

		int res = 0;
		CT_XG_PARAM tab;

		tab.init(webx::GetDBConnect());

		tab.id = id;
		tab.statetime.update();

		if (name.length() > 0) tab.name = name;
		if (param.length() > 0) tab.param = param;
		if (filter.length() > 0) tab.filter = filter;
		if (remark.length() > 0) tab.remark = remark;

		if (flag == "U")
		{
			res = tab.update();
		}
		else if (flag == "D")
		{
			res = tab.remove();
		}
		else if (flag == "A")
		{
			if (param.empty() && filter.length() > 2)
			{
				vector<string> vec;

				filter = stdx::trim(filter, "()");

				if (stdx::split(vec, filter, ",") > 0) tab.param = stdx::trim(vec[0]);
			}
			
			if (tab.find() && tab.next()) return simpleResponse(XG_DUPLICATE);

			res = tab.insert();
		}
		else
		{
			res = XG_PARAMERR;
		}

		if (res > 0)
		{
			sp<Session> session = webx::GetLocaleSession("SYSTEM_PARAMETER");

			if (session) session->remove(id);
		}

		return simpleResponse(res);
	}

	webx::PrintRecordview(out, "${parameter}");
%>

<script>
{
	var parameteraccess = getAccess('${filename}');

	$recordvmdata.button = [{
		title: '编辑',
		click: editParamRecord,
		disable: parameteraccess < 0
	},{
		title: '删除',
		click: removeParamRecord,
		disable: parameteraccess < 0
	}];

	$('#ReloadConfigButton').click(function(){
		showConfirmMessage('是否要重新加载配置文件？', '更新配置', function(flag){
			if (flag){
				showToastMessage('正在重新加载配置文件...', true);

				getHttpResult('/execmodule', {cmd: 'reload'}, function(data){
					showToast('加载配置文件成功');
				});
			}
		});
	});

	function addParamRecord(){
		var data = {
			title: ['参数编号', '参数名称', '参数过滤', '参数备注'],
			model: {id: '', name: '', filter: '', remark: ''},
			style: [
				{size: 24, minlength: 4, maxlength: 32},
				{size: 24, minlength: 4, maxlength: 32},
				{size: 24, minlength: 0, maxlength: 128, type: 'textarea'},
				{size: 24, minlength: 0, maxlength: 128, type: 'textarea'}
			]
		};

		var elem = showConfirmDialog(data, '添加参数', function(flag){
			if (flag){
				data.model['flag'] = 'A';
				
				getHttpResult('/${filename}', data.model, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code == XG_DUPLICATE){
						showToast('参数已存在');
					}
					else if (data.code < 0){
						showToast('参数添加失败');
					}
					else{
						$recordvmdata.reload();
					}
				});
			}
		});

		$(elem.remark).width($(elem.id).width());
		$(elem.filter).width($(elem.id).width()).attr('placeholder', "输入以'/'开头正则表达式或取值范围如(0,1,2,3,4...)");
	}

	function removeParamRecord(item){
		showConfirmMessage('是否要删除[' + item.name + ']参数？', '删除参数', function(flag){
			if (flag){
				getHttpResult('/${filename}', {flag: 'D', id: item.id}, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除参数失败');
					}
					else{
						$recordvmdata.reload();
					}
				});
			}
		});
	}

	function editParamRecord(item){
		var vmdata = {
			title: ['参数内容', '参数备注'],
			model: {param: item.param, remark: item.remark},
			style: [
				{minlength: 1, maxlength: 128, type: 'textarea'},
				{minlength: 1, maxlength: 128, type: 'textarea'}
			]
		};

		var filter = item.filter;

		if (filter.length > 3 && filter.substr(0, 1) == '(' && filter.substr(filter.length - 1) == ')'){
			var valist = filter.substr(1, filter.length - 2).split(',');
			if (valist.length > 0) vmdata.style[0] = {select: valist.join('|')};
		}

		showConfirmDialog(vmdata, '修改参数', function(flag){
			if (flag){
				vmdata.model['flag'] = 'U';
				vmdata.model['id'] = item.id;

				getHttpResult('/${filename}', vmdata.model, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('修改参数失败');
					}
					else{
						$recordvmdata.reload();
					}
				});
			}
		});
	}
}
</script>