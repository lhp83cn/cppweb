<%@ path=${filename}%>

<div></div>

<%
	param_string(flag);
	param_name_string(tabid);

	auto add = [&](){
	};

	auto remove = [&](){
	};

	auto update = [&](){
	};

	if (flag.length() > 0)
	{
		checkLogin();
		checkSystemRight();

		if (flag == "A")
		{
			add();
		}
		else if (flag == "D")
		{
			remove();
		}
		else if (flag == "U")
		{
			update();
		}

		return simpleResponse(XG_OK);
	}

	webx::PrintRecordview(out, tabid);
%>

<!-- script>
{
	function remove(item){
	}

	function update(item){
	}

	$recordvmdata.button = [{
		title: '编辑',
		color: '#009',
		click: update
	}, {
		title: '删除',
		color: '#C00',
		click: remove
	}];
}
</script -->