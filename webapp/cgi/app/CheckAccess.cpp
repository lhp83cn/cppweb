#include <webx/route.h>
#include <webx/route.h>

class CheckAccess : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(CheckAccess)

int CheckAccess::process()
{
	param_string(path);

	size_t pos;
	string param;
	string grouplist;
	vector<string> vec = stdx::split(path, ",");

	try
	{
		checkLogin();

		grouplist = token->getGrouplist();
	}
	catch(Exception e)
	{
	}

	int res = 0;
	JsonElement arr = json.addArray("list");

	for (string& item : vec)
	{
		if ((pos = stdx::trim(item).find('?')) == string::npos)
		{
			param.clear();
			path = item;
		}
		else
		{
			param = item.substr(pos + 1);
			path = item.substr(0, pos);
		}

		JsonElement data = arr[res++];

		try
		{
			data["access"] = webx::CheckAccess(path, param, grouplist);
		}
		catch(Exception e)
		{
			data["access"] = e.getErrorCode();
		}

		data["path"] = item;
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}