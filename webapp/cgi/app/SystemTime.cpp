#include <webx/route.h>

class SystemTime : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(SystemTime)

int SystemTime::process()
{
	param_string(flag);

	if (flag == "S")
	{
		out << DateTime::ToString();
	}
	else
	{
		json["datetime"] = DateTime::ToString();
		json["code"] = XG_OK;
		out << json;
	}
	
	return XG_OK;
}