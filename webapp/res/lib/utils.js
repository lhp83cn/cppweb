﻿var vue = null;
var lang = 'CN';
var curuser = null;
var machinetype = 'PC';
var browserinfo = null;
var mousemovevent = null;
var logincallback = null;
var toasthidetimer = null;
var contextmenuview = null;
var styleloadmap = {};
var scriptloadmap = {};
var autoresizemap = {};
var automouseupmap = {};
var singletontimermap = {};
var contextmenuattach = null;
var defaultcontextmenu = document.oncontextmenu;

var XG_OK = 1;
var XG_FAIL = 0;
var XG_ERROR = -1;
var XG_IOERR = -2;
var XG_SYSERR = -3;
var XG_NETERR = -4;
var XG_TIMEOUT = -5;
var XG_DATAERR = -6;
var XG_SYSBUSY = -7;
var XG_PARAMERR = -8;
var XG_NOTFOUND = -9;
var XG_NETCLOSE = -10;
var XG_NETDELAY = -11;
var XG_SENDFAIL = -12;
var XG_RECVFAIL = -13;
var XG_AUTHFAIL = -14;
var XG_DAYLIMIT = -15;
var XG_DUPLICATE = -16;
var XG_UNINSTALL = -17;
var XG_NOTCHANGED = -18;
var XG_DETACHCONN = -999999;

var getSequence = ((
	function(){
	var id = 0; 
	return function(){
		return ++id;
	};
})());

var commonfilter = {
	name: function(text){
		return new RegExp(/^\w+$/).test(text) ? text : '';
	}
};

$.extend({
	pack: function(elem){
		if (isObject(elem)){
			if (isFunction(elem.attr)) return elem;
		}
		else{
			elem = getString(elem);
			var ch = elem.charAt(0);
			if (ch == '_') return $('#' + elem);
			if (ch >= 'a' && ch <= 'z') return $('#' + elem);
			if (ch >= 'A' && ch <= 'Z') return $('#' + elem);
			if (ch >= '0' && ch <= '9') return $('#' + elem);
		}

		return $(elem);
	}
});

Array.prototype.unique = function(){
	return Array.from(new Set(this));
}

Array.prototype.index = function(obj){
	for (var i = 0; i < this.length; i++){
		if (obj == this[i]) return i;
	}
	return -1;
}

Array.prototype.pop_front = function(count){
	if (typeof(count) === 'undefined') this.splice(0, 1);
	if (typeof(count) === 'number') this.splice(0, count);
}

Array.prototype.push_front = function(){
	for (var i = 0; i < arguments.length; i++) this.splice(i, 0, arguments[i]);
}

String.prototype.colorHex = function(){
	var res = '#';
	var that = this;
	if(/^(rgb|RGB)/.test(that)){
		var color = that.replace(/(?:\(|\)|rgb|RGB)*/g, '').split(',');
		for (var i = 0; i < color.length; i++){
			var hex = Number(color[i]).toString(16);
			if (hex === '0') hex += hex;	
			res += hex;
		}
		return res.length == 7 ? res : that;
	}
	else{
		var num = that.replace(/#/, '').split('');
		if (num.length === 6) return that;	
		if (num.length === 3){
			for (var i = 0; i < num.length; i += 1) res += num[i] + num[i];
			return res;
		}
	}
	return that;
};

String.prototype.replaceAll = function(src, dest){
    return this.replace(new RegExp(src, 'gm'), dest);
}

var IconPicker = function(elem, path){
	 this.datalist = getIconList(path);
	 this.initialize(getCtrl(elem));
}

IconPicker.prototype = {
	initialize: function(elem){
		var count = 0;
		var self = this;
		var pack = $('#IconPickerTable');
		var html = "<table class='IconPickerTable' id='IconPickerTable'>";

		if (pack && strlen(pack.html()) > 64 && navigator.userAgent.indexOf('Firefox') >= 0){
			this.div = pack.parent()[0];
		}
		else{
			var cols = (this.datalist.length > 20) ? 8 : 6;
			var rows = Math.ceil(this.datalist.length / cols);

			for (i = 0; i < rows; i++){
				html += "<tr>";

				for (j = 0; j < cols && count < this.datalist.length; j++){
					html += '<td style="background-image:url(' + this.datalist[count] + ')"></td>';
					count++;
				}
			   
				html+= "</tr>";
			}
			
			html += '</table>';

			this.div = document.createElement('div');
			this.div.innerHTML = html;
		}

		this.trigger = elem;
		
		var sz = this.trigger.clientWidth;
		var tds = this.div.getElementsByTagName('td');

		for (var i = 0, l = tds.length ; i < l; i++){
			tds[i].onmousedown = function(e){
				self.setIcon(self.trigger, this.style.backgroundImage);
				self.trigger.blur();
				e.stopPropagation();
			}

			if (tds[i].style.backgroundImage == self.trigger.style.backgroundImage){
				tds[i].style.backgroundColor = 'red';
			}
			else{
				tds[i].style.backgroundColor = '#FFF';
			}
			
			tds[i].style.margin = '0px';
			tds[i].style.padding = '0px';
			tds[i].style.width = sz + 'px';
			tds[i].style.height = sz + 'px';
		}

		this.trigger.parentNode.appendChild(this.div);
		this.div.style.zIndex = 1000;
		this.div.style.position = 'fixed';
		this.div.style.left = getLeft(this.trigger) + 'px'
		this.div.style.top = (this.trigger.clientHeight + getTop(this.trigger) + 1) + 'px';
		this.trigger.onclick = function(){
			if(self.div.style.display == 'none'){
				self.show();
			}
			else{
				self.hide();
		   }
		}
	},
	
	setIcon : function(trigger, color){
		trigger.style.backgroundImage = color;
		if (strlen(trigger.value) > 0){
			trigger.value = color.colorHex().toUpperCase();
		}
		this.hide();
	},
	
	hide : function(){
		this.div.style.display = 'none'
	},
	
	show : function(){
		this.div.style.display = 'block'
	}
}

var ColorPicker = function(elem){
	 this.datalist = [
			'#000000','#993300','#333300','#003300','#003366','#000080','#333399','#333333',
			'#800000','#FF6600','#808000','#008000','#008080','#0000FF','#666699','#808080',
			'#FF0000','#FF9900','#99CC00','#339966','#33CCCC','#3366FF','#800080','#999999',
			'#FF00FF','#FFCC00','#FFFF00','#00FF00','#00FFFF','#00CCFF','#993366','#CCCCCC',
			'#FF99CC','#FFCC99','#FFFF99','#CCFFCC','#CCFFFF','#99CCFF','#CC99FF','#FFFFFF'];
	 this.initialize(getCtrl(elem));
}

ColorPicker.prototype = {
	initialize: function(elem){
		var count = 0;
		var self = this;
		var pack = $('#ColorPickerTable');
		var html = "<table class='ColorPickerTable' id='ColorPickerTable'>";

		if (pack && strlen(pack.html()) > 64 && navigator.userAgent.indexOf('Firefox') >= 0){
			this.div = pack.parent()[0];
		}
		else{
			var cols = 8;
			var rows = Math.ceil(this.datalist.length / cols);

			for (i = 0; i < rows; i++){
				html += "<tr>";

				for (j = 0; j < cols && count < this.datalist.length; j++){
					html += '<td style="text-shadow:0px 1px 1px #CCBBAA;background:' + this.datalist[count]+ '"></td>';
					count++;
				}
			   
				html+= "</tr>";
			}

			html += '</table>';

			this.div = document.createElement('div');
			this.div.innerHTML = html;
		}
		
		this.trigger = elem;
		
		var tds = this.div.getElementsByTagName('td');

		for (var i = 0, l = tds.length ; i < l; i++){
			tds[i].onmousedown = function(e){
				self.setColor(self.trigger, this.style.backgroundColor);
				self.trigger.blur();
				e.stopPropagation();
			}

			if (tds[i].style.backgroundColor == self.trigger.style.backgroundColor){
				tds[i].innerHTML = '&#10004';
			}
			else{
				tds[i].innerHTML = '';
			}

			tds[i].style.margin = '0px';
			tds[i].style.padding = '0px';
		}
		this.trigger.parentNode.appendChild(this.div);
		this.div.style.zIndex = 1000;
		this.div.style.position = 'fixed';
		this.div.style.left = getLeft(this.trigger) + 'px'
		this.div.style.top = (this.trigger.clientHeight + getTop(this.trigger) + 1) + 'px';
		this.trigger.onclick = function(){
			if(self.div.style.display == 'none'){
				self.show();
			}
			else{
				self.hide();
		   }
		}
	},
	
	setColor : function(trigger, color){
		trigger.style.backgroundColor = color;
		if (strlen(trigger.value) > 0){
			trigger.value = color.colorHex().toUpperCase();
		}
		this.hide();
	},
	
	hide : function(){
		this.div.style.display = 'none'
	},
	
	show : function(){
		this.div.style.display = 'block'
	}
}

var AutoTextPicker = function(elem, words){
	 this.datalist = words;
	 this.initialize(getCtrl(elem));
}

AutoTextPicker.prototype = {
	count: 0,
	index: -1,
	onclick: null,
	onselect: null,
	selected: null,
	initialize: function(elem){
		var cols = 1;
		var self = this;
		var rows = Math.ceil(this.datalist.length / cols);
		var html = "<table id='AutoTextPickerTable' width='100%'>";

		for (i = 0; i < rows; i++){
			html += "<tr class='AutoDataRow'>";

			for (j = 0; j < cols && self.count < this.datalist.length; j++){
				html += '<td>'+ this.datalist[self.count] + '</td>';
				self.count++;
			}
		   
			html+= "</tr>";
		}

		html += '</table>';

		this.div = document.createElement('div');
		this.div.innerHTML = html;
		
		this.trigger = elem;
		this.trigger.parentNode.appendChild(this.div);
		
		this.updateStatus(self);
		
		this.div.style.zIndex = 1000;
		this.div.style.overflow = 'auto';
		this.div.style.position = 'fixed';
		this.div.style.border = '1px solid #ABC';
		this.div.style.left = getLeft(this.trigger) + 'px';
		this.div.style.width = this.trigger.clientWidth + 'px';
		this.div.style.top = (this.trigger.clientHeight + getTop(this.trigger) + 2) + 'px';
		
		this.trigger.onclick = function(){
			if(self.div.style.display == 'none'){
				self.show();
			}
			else{
				self.hide();
		   }
		};
		
		this.trigger.onkeydown = function(e){
			if (e.keyCode == 13){
				self.hide();
			}
			else if (e.keyCode == 38){
				self.selectItem(self, -1);
				e.preventDefault();
			}
			else if (e.keyCode == 40){
				self.selectItem(self, 1);
				e.preventDefault();
			}
		};
	},
	
	selectItem : function(self, step){
		self.index += step;
		
		if (self.index < 0) self.index = self.count - 1;
		if (self.index >= self.count) self.index = 0;

		var tds = self.div.getElementsByTagName('td');

		for (var i = 0, l = tds.length ; i < l; i++){
			if (self.index == i){
				self.setText(self.trigger, tds[i].innerHTML);
				break;
			}
		}

		self.show();
	},
	
	destroy : function(){
		$(this.div).remove();
	},
	
	updateStatus : function(self){
		var tds = self.div.getElementsByTagName('td');

		for (var i = 0, l = tds.length ; i < l; i++){
			$(tds[i]).attr('index', i);

			tds[i].onmousedown = function(){
				self.setText(self.trigger, this.innerHTML);
				self.index = parseInt($(this).attr('index'));
				self.trigger.blur();
				
				if (self.onclick) self.onclick(this.innerHTML);
			}
			
			if (self.selected){
				if (self.selected(tds[i].innerHTML, self.trigger.value)){
					tds[i].style.background = '#BCD';
					tds[i].style.color = '#800';
					self.index = i;
				}
				else{
					tds[i].style.background = '#FFF';
					tds[i].style.color = '#234';
				}
			}
			else{
				if (tds[i].innerHTML == self.trigger.value){
					tds[i].style.background = '#BCD';
					tds[i].style.color = '#800';
					self.index = i;
				}
				else{
					tds[i].style.background = '#FFF';
					tds[i].style.color = '#234';
				}
			}
		}
		
		$('#AutoTextPickerTable td').css('margin', '0px');
		$('#AutoTextPickerTable td').css('cursor', 'default');
		$('#AutoTextPickerTable td').css('padding', $(self.trigger).css('padding'));
		$('#AutoTextPickerTable td').css('font-size', $(self.trigger).css('font-size'));
	},
	
	setText : function(trigger, text){
		this.hide();
		trigger.value = text;
		if (onselect) onselect(text);
	},
	
	hide : function(){
		this.div.style.display = 'none';
	},
	
	show : function(){
		this.div.style.display = 'block';
		this.updateStatus(this);
	}
}

function getHost(){
	var host = window.location.href;
	var pos = host.indexOf('/', 8);
	return pos < 0 ? host : host.substring(0, pos);
}

function getSpace(){
	return '';
}

function getLanguage(){
	return lang;
}

function clearCookie(){
	var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
	if (keys){
		var i = keys.length;
		while (i--){
			document.cookie = keys[i]+'=0;expires=' + new Date(0).toUTCString();
		}
		setCurrentUser(null);
	}
}

function getClientWidth(){
	return document.documentElement.clientWidth || document.body.clientWidth;
}

function getClientHeight(){
	return document.documentElement.clientHeight || document.body.clientHeight;
}

function clearStorage(){
	localStorage.clear();
}

function getStorage(key){
	return localStorage.getItem(key);
}

function removeStorage(key){
	localStorage.removeItem(key);
}

function setStorage(key, val){
	localStorage.setItem(key, val);
}

function clearSession(){
	sessionStorage.clear();
}

function getSession(key){
	return sessionStorage.getItem(key);
}

function removeSession(key){
	sessionStorage.removeItem(key);
}

function setSession(key, val){
	sessionStorage.setItem(key, val);
}

function setClipboard(text, toast){
	if (typeof(ClipboardJS) == 'undefined'){
		loadScript('/res/lib/clipboard.js.gzip');
	}

	var board = new ClipboardJS('body', {
		text: function(){
			if (toast) showToast(toast);
			board.destroy();
			return text;
		}
	});
}

function removeResizeCallback(name){
	delete autoresizemap[name];
}

function setResizeCallback(name, func){
	autoresizemap[name] = func;
}

function removeMouseupCallback(name){
	delete automouseupmap[name];
}

function setMouseupCallback(name, func){
	automouseupmap[name] = func;
}

function clearSingletonInterval(name){
	if (name == null){
		for (var name in singletontimermap) clearInterval(singletontimermap[name]);
		singletontimermap = {};
	}
	else{
		if (name in singletontimermap){
			clearInterval(singletontimermap[name]);
			delete singletontimermap[name];
		}
	}
}

function setSingletonInterval(name, delay, func){
	if (name in singletontimermap) clearInterval(singletontimermap[name]);
	singletontimermap[name] = setInterval(func, delay);
}

function getParameter(name){
   var reg = new RegExp('(^|&)'+ name +'=([^&]*)(&|$)');
   var res = window.location.search.substr(1).match(reg);
   return res ? decodeURIComponent(res[2]) : null;
}

function getFileName(path){
	var pos = path.lastIndexOf('/');
	return pos >= 0 ? path.substring(pos + 1) : path;
}

function getFileExtname(path){
	var pos = path.lastIndexOf('.');
	return pos >= 0 ? path.substring(pos + 1).toLowerCase() : '';
}

function isFileName(name, required, maxlen){
	var len = strlen(name);

	if (len == 0) return required ? false : true;

	if (maxlen == null) maxlen = 64;

	var errstr = "`'^*<>,:;?|%\r\n\t\"\\/";

	if (len > maxlen) return false;

	for (var i = 0; i < len; i++){
		if (errstr.indexOf(name[i]) >= 0) return false;
	}

	return true;
}

function isFilePath(name, required, maxlen){
	var len = strlen(name);

	if (len == 0) return required ? false : true;

	if (maxlen == null) maxlen = 64;

	var errstr = "`'^*<>,:;?|%\r\n\t\"";

	if (len > maxlen) return false;

	for (var i = 0; i < len; i++){
		if (errstr.indexOf(name[i]) >= 0) return false;
	}

	return true;
}

function getDateTimeString(date){
	if (date == null) date = new Date();

	return date.getFullYear() + '-' + getString(100 + date.getMonth() + 1).substr(1)
			+ '-' + getString(100 + date.getDate()).substr(1)
			+ ' ' + getString(100 + date.getHours()).substr(1)
			+ ':' + getString(100 + date.getMinutes()).substr(1)
			+ ':' + getString(100 + date.getSeconds()).substr(1);
}


function getDateTimeStringFromId(id){
	return id.substr(0, 4) + '-' + id.substr(4, 2) + '-' + id.substr(6, 2) + ' ' + id.substr(8, 2) + ':' + id.substr(10, 2) + ':' + id.substr(12, 2);
}

function getShortDateTimeString(date){
	if (date == null) date = new Date();

	return date.getFullYear() + getString(100 + date.getMonth() + 1).substr(1)
			+ getString(100 + date.getDate()).substr(1)
			+ getString(100 + date.getHours()).substr(1)
			+ getString(100 + date.getMinutes()).substr(1)
			+ getString(100 + date.getSeconds()).substr(1);
}

function dispatch(elem, name){
	var event = document.createEvent('Event');
	event.initEvent(name, true, true);
	getCtrl(elem).dispatchEvent(event);
}

function strlen(str){
	if (str == null) return 0;

	var ch;
	var len = 0;
	var val = str + '';
	
	for (var i = 0; i < val.length; i++){
		ch = val.charCodeAt(i);
		if ((ch >= 0x0001 && ch <= 0x007E) || (0xFF60 <= ch && ch <= 0xFF9F)){
			len++;
		}
		else{
			len += 2;
		}
	}
	
	return len;
}

function markdown(msg){
	var str = 0;
	var end = 0;
	var res = '';
	var src = msg;
	var tmp = null;
	var updated = false;
	var linetag = 'falryepqnfapfywrwd';
	
	while (true){
		str = msg.indexOf('```');
		if (str < 0){
			res += msg;
			break;
		}

		str += 3
		res += msg.substr(0, str);

		end = msg.indexOf('```', str);
		if (end < 0){
			res += msg.substr(str);
			updated = false;
			break;
		}

		tmp = msg.substring(str, end += 3).replace(new RegExp('\n\t', 'gm'), '\n\t\t');
		tmp = tmp.replace(new RegExp('\n--', 'gm'), '\n#' + linetag);
		tmp = tmp.replace(new RegExp('\n', 'gm'), linetag + '\n');
		msg = msg.substr(end);
		updated = true;
		res += tmp;
	}

	if (typeof(marked) == 'undefined'){
		loadScript('/res/lib/marked.js.gzip');
	}

	if (updated){
		msg = marked(res).replace(new RegExp('#' + linetag, 'gm'), '--');
		msg = msg.replace(new RegExp(linetag, 'gm'), '');
	}
	else{
		msg = marked(src);
	}

	return "<div class='MarkdownContentDiv'>" + msg + "</div>";
}

function setMarkedOptions(option){
	if (typeof(marked) == 'undefined'){
		loadScript('/res/lib/marked.js.gzip');
	}
	marked.setOptions(option);
}

function isArray(val){
	return Array.isArray(val);
}

function isObject(val){
	return typeof(val) == 'object';
}

function isString(val){
	return typeof(val) == 'string';
}

function isFunction(val){
	return typeof(val) == 'function';
}

function getString(val){
	if (strlen(val) == 0) return '';
	return '' + val;
}

function htmlEscape(html){
	return html.replace(/[<>&"]/g, function(c){
		return {
			'<': '&lt;',
			'>': '&gt;',
			'&': '&amp;',
			'"': '&quot;'
		}[c];
	});
}

function centerText(text, weight){
	if (weight) return "<div style='text-align:center;font-weight:" + weight + "'>" + text + "</div>";
	else return "<div style='text-align:center'>" + text + "</div>";
}

function loadStyle(url){
	var elem = styleloadmap[url];
	if (elem) return elem;
	elem = document.createElement('link');
	elem.href = url;
	elem.type = 'text/css';
	elem.rel = 'stylesheet';
	document.getElementsByTagName('head')[0].appendChild(elem);
	return styleloadmap[url] = elem;
}

function loadScript(url, async){
	var elem = scriptloadmap[url];
	if (elem) return elem;
	elem = document.createElement('script');
	elem.type = 'text/javascript';
	if (async){
		elem.src = url;
	}
	else{
		$(elem).html(getHttpResult(url));
	}
	document.getElementsByTagName('head')[0].appendChild(elem);
	return scriptloadmap[url] = elem;
}

function checkEmail(mail){
	var reg = /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
	return reg.test(mail);
}

function getTextSize(text){
	var sz = strlen(text);

	if (machinetype == 'PC'){
		return sz > 0 ? sz : 1;
	}
	else{
		return sz > 0 ? sz : 1;
	}
}

function setLanguage(language){
	if (strlen(language) > 0){
		lang = (language == 'CN' ? 'CN' : 'EN');
	}
}

function setLabelText(elem, text){
	$.pack(elem).attr('size', getTextSize(text)).val(text);
}

function getBrowserInfo(){
	if (browserinfo) return browserinfo;

	var ua = navigator.userAgent.toLowerCase();
	var msie = (ua.match(/firefox|chrome|safari|opera/g) || 'other')[0];
	
	if ((ua.match(/msie|trident/g) || [])[0]) msie = 'msie';

	var pc;
	var plat;
	var prefix;

	if ('ontouchstart' in window || ua.indexOf('touch') !== -1 || ua.indexOf('mobile') !== -1){
		if (ua.indexOf('ipad') >= 0){
			pc = 'pad';
		}
		else if (ua.indexOf('mobile') >= 0){
			pc = 'mobile';
		}
		else if (ua.indexOf('android') >= 0){
			pc = 'android';
		}
		else{
			pc = 'pc';
		}
	}
	else{
		pc = 'pc';
	}
	
	machinetype = pc.toUpperCase();

	switch (msie){
		case 'chrome':
		case 'safari':
		case "mobile":
			prefix = 'webkit';
			break;
		case 'msie':
			prefix = 'ms';
			break;
		case 'firefox':
			prefix = 'moz';
			break;
		case 'opera':
			prefix = 'opera';
			break;
		default:
			prefix = 'webkit';
			break
	}

	plat = ua.indexOf('android') > 0 ? 'android' : navigator.platform.toLowerCase();
	
	browserinfo = {
		version: (ua.match(/[\s\S]+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
		plat: plat,
		type: msie,
		prefix: prefix,
		machinetype: machinetype
	};

	return browserinfo;
}

function getContextMenuAttach(){
	return contextmenuattach;
}

function bindContextMenu(elem, menu){
	let view = getCtrl(menu);
	let attach = getCtrl(elem);

	view.onclick = function(){
		document.oncontextmenu = defaultcontextmenu;
		view.style.display = 'none';
	}

	view.onclick();

	attach.onmouseup = function(e){
		e.stopPropagation();

		if (e.button == 2){
			document.oncontextmenu = function(e){
				e.preventDefault();
			}

			var x = e.clientX - 1;
			var y = e.clientY - 1;

			view.style.cssText = 'z-index:10000;position:fixed;display:block;left:' + x + 'px;' + 'top:' + y + 'px;';
			contextmenuattach = elem;
			contextmenuview = menu;
		}
	}
}

function getDateTime(diff, seperator){
	var now = new Date();
	
	if (diff) now.setDate(now.getDate() + diff);

	var len = strlen(seperator);
	var D = now.getDate();
	var M = now.getMonth();
	var h = now.getHours();
	var m = now.getMinutes();
	var s = now.getSeconds();

	if (len == 0){
		seperator = '- :';
	}
	else if (len == 1){
		seperator = seperator + ' :';
	}
	else if (len == 2){
		seperator = seperator + ':';
	}
	
	M = M + 1;

	if (D < 10) D = '0' + D;
	if (M < 10) M = '0' + M;
	if (h < 10) h = '0' + h;
	if (m < 10) m = '0' + m;
	if (s < 10) s = '0' + s;
	
	return now.getFullYear() + seperator.charAt(0) + M + seperator.charAt(0) + D + seperator.charAt(1) + h + seperator.charAt(2) + m + seperator.charAt(2) + s;
}

function getAmountString(val){
	if (strlen(val) == 0) return '0.00';
	if (getString(val).indexOf('.') < 0) return val + '.00';
	if ((val = parseFloat(getString(val))) == null) return '0.00';

	val = getString(val);
	
	var pos = val.indexOf('.');
	
	if (pos > 0){
		var len = strlen(val.substring(pos + 1));

		if (len == 2) return val;
		else if (len == 1) return val + '0';
		else if (len == 0) return val + '00';
		else return val.substring(0, pos + 3);
	}

	return val + '.00';
}

function getCtrl(elem){
	return $.pack(elem)[0];
}

function appendCtrl(elem){
	var ctrl = getCtrl(elem);
	if (getCtrl(ctrl.id) == null) document.body.appendChild(ctrl);
	return getCtrl(ctrl.id);
}

function createCtrl(tag, id){
	if (id == null) return document.createElement(tag);
	var ctrl = getCtrl(id);
	if (ctrl == null){
		ctrl = document.createElement(tag);
		ctrl.id = id;
	}
	return ctrl;
}

function getTop(elem){
	var rect = getCtrl(elem).getBoundingClientRect(); 
	return rect.top - document.documentElement.clientTop; 
}

function getLeft(elem){
	var rect = getCtrl(elem).getBoundingClientRect(); 
	return rect.left - document.documentElement.clientLeft; 
}

function getWidth(elem){
	return $.pack(elem).width();
}

function getHeight(elem){
	return $.pack(elem).height();
}

function setWidth(elem, width){
	return $.pack(elem).width(width);
}

function setHeight(elem, height){
	return $.pack(elem).height(height);
}

function isJsonString(str){
	try{
		JSON.parse(str);
		return true;
	}
	catch(e){
		return false;
	}
}

function getIconList(path){
	var iconlist = [];

	getHttpResult('/geticonlist', {path: getString(path)}, function(data){
		if (data.code > 0) iconlist = data.list;
	});

	return iconlist;
}

function getMapFromList(list, field){
	let map = new Map();
	if (isArray(list) && list.length > 0){
		list.forEach(item => {
			if (isFunction(field)){
				map.set(field(item), item);
			}
			else{
				map.set(item[field], item);
			}
		});
	}
	return map;
}

function graspUserInfo(list, field, func){
	if (field == null){
		field = 'user';
	}
	else if (isFunction(field)){
		if (func == null){
			func = field;
		}
		field = 'user';
	}
	if (isArray(list)){
		if (list.length > 0){
			let arr = [];
			list.forEach(item => {
				arr.push(item[field]);
			});
			getHttpResult('/getuserlist', {user: arr.unique().join(',')}, function(data) {
				list.forEach(item => {
					for (let i = 0; i < data.list.length; i++) {
						if (item[field] == data.list[i].user){
							if (func){
								func(item, data.list[i]);
							}
							else{
								item['$user'] = data.list[i];
							}
							break;
						}
					}
				})
			})
		}
	}
	else{
		getHttpResult('/getuserlist', {user: list[key]}, function(data){
			if (func){
				func(list, data.list[0]);
			}
			else{
				list['$user'] = data.list[0];
			}
		});
	}
}

function graspCanvas(elem, scale, func){
	elem = getCtrl(elem);

	if (scale == null) scale = 1;

	var canvas = document.createElement('canvas');
	var width = elem.offsetWidth;
	var height = elem.offsetHeight;
	var context = canvas.getContext('2d');

	canvas.width = width * scale;
	canvas.height = height * scale;
	context.scale(scale, scale);

	var opts = {
		dpi: window.devicePixelRatio * scale,
		scale: 1,
		canvas: canvas,
		useCORS: true
	};

	html2canvas(elem, opts).then(func);
}

function getCursorLocation(){
	if (mousemovevent == null){
		document.onmousemove = function(e){
			mousemovevent = e || window.event;
		}

		return null;
	}

	var ev = mousemovevent;
	
	if (ev.pageX || ev.pageY){
		return {x: ev.pageX, y: ev.pageY};
	}
	
	return {
		x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y: ev.clientY + document.body.scrollTop - document.body.clientTop
	};
}

function getBackgroundImage(elem){
	var icon = $.pack(elem).css('background-image');

	if (icon.indexOf('/res') >= 0){
		icon = icon.substring(icon.indexOf('/res'));
	}
	else if (icon.indexOf('/app') >= 0){
		icon = icon.substring(icon.indexOf('/app'));
	}
	else if (icon.indexOf('\'') >= 0){
		icon = icon.substring(icon.indexOf('\'') + 1);
	}
	else if (icon.indexOf('\"') >= 0){
		icon = icon.substring(icon.indexOf('\"') + 1);
	}
	
	icon = icon.substring(0, icon.lastIndexOf(')')) || icon;
	icon = icon.substring(0, icon.lastIndexOf('\'')) || icon;
	icon = icon.substring(0, icon.lastIndexOf('\"')) || icon;
	
	return icon;
}

function getVue(elem, data, method, filter){
	return new Vue({el: '#' + getCtrl(elem).id, data: data, methods: method, filters: filter});
}

function checkHttpResult(data, title, quiet){
	var res = false;

	if (strlen(title) <= 0){
		title = lang == 'CN' ? '处理' : 'process';
	}

	if (isObject(data)){
		if (data.$code == 401){
			sessionTimeout();
			return false;
		}

		if (data.$code == 403){
			showNoAccessToast();
			return false;
		}

		if (typeof(data.code) == 'undefined'){
			res = data.$code == 200;
		}
		else{
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
				return false;
			}

			if (data.code == XG_AUTHFAIL){
				showNoAccessToast();
				return false;
			}

			if (data.code == XG_DAYLIMIT){
				showErrorToast(lang == 'CN' ? '不要频繁操作' : 'operate frequently');
				return false;
			}

			res = data.code >= 0;
		}
	}

	if (res){
		if (quiet) return res;

		showToast(title + (lang == 'CN' ? '成功' : ' success'));
	}
	else{
		showErrorToast(title + (lang == 'CN' ? '失败' : ' failed'));
	}

	return res;
}

function getHttpResult(url, data, func, async, json, cache){
	var result = {code: XG_NETERR};
	var contype = 'application/x-www-form-urlencoded';

	if (json){
		contype = 'application/json';
		data = JSON.stringify(data);
	}

	if (cache == null){
		var ext = getFileExtname(url).toLowerCase();
		if (ext == 'js' || ext == 'css' || ext == 'htm' || ext == 'html' || ext == 'gzip'){
			cache = true;
		}
		else{
			cache = false;
		}
	}

	$.ajax({
		url: url,
		data: data,
		cache: cache,
		contentType: contype,
		type: data ? 'POST' : 'GET',
		async: async ? true : false,
		success: function(msg){
			try{
				result = JSON.parse(msg);
				result['$desc'] = 'OK';
				result['$code'] = 200;
			}
			catch(e){
				result = msg;
			}

			if (func) func(result, msg, 200, 'OK');
		},
		error: function(data, message){
			try{
				result = JSON.parse(data.responseText);
				result['$desc'] = data.statusText;
				result['$code'] = data.status;
			}
			catch(e){
				result = data.responseText;
			}

			if (func) func(result, data.responseText, data.status, data.statusText);
		}
	});

	return async ? true : result;
}

function getAccess(path){
	var arr = [];
	var flag = isArray(path);

	if (flag){
		for (var i = 0; i < path.length; i++) arr.push(0);
	}
	else{
		arr.push(0);
	}

	getHttpResult('/checkaccess', {path: flag ? path.join(',') : path}, function(data){
		if (data.code > 0){
			for (var i = 0; i < data.list.length; i++){
				arr[i] = data.list[i].access;
			}
		}
	});

	return flag ? arr : arr[0];
}

function getAccessMap(path){
	var map = {};
	var arr = getAccess(path);

	if (isArray(path)){
		for (var i = 0; i < path.length; i++){
			map[path[i]] = arr[i];
		}
	}
	else{
		map[path] = arr;
	}

	return map;
}

function MoveFunc(elem){
	elem = getCtrl(elem);

	var result = {x : elem.offsetLeft, y : elem.offsetTop};

	elem = elem.offsetParent;
	
	while (elem){
		result.x += elem.offsetLeft;
		result.y += elem.offsetTop;
		elem = elem.offsetParent;
	}
	
	return result;	  
};

function Endrag(source, target){
	var x0 = 0, y0 = 0, x1 = 0, y1 = 0;
	var moveable = false, NS = navigator.appName == 'Netscape';
	
	source = getCtrl(source);
	target = getCtrl(target);
   
	source.style.cursor = 'move';
	
	source.onmousedown = function(e){
		e = e || window.event;

		if (e.button == NS ? 0 : 1){
			if (!NS) this.setCapture();
			x0 = e.clientX ; 
			y0 = e.clientY ; 
			x1 = parseInt(MoveFunc(target).x); 
			y1 = parseInt(MoveFunc(target).y);   
			moveable = true;
		}
	};
	
	source.onmousemove = function(e){
		e = e || window.event;
		
		if (moveable){
			target.style.top = (y1 + e.clientY - y0) + 'px';
			target.style.left = (x1 + e.clientX - x0) + 'px';
		}
	};

	source.onmouseup = function(e){
		if (moveable){
			if (!NS) this.releaseCapture();
			moveable = false; 
		}
	};
}

function hideMsgBox(){
	$('#XG_MSGBOX_DIV_ID').hide();
	$('#XG_MODESCREEN_DIV_ID').fadeOut();
}

function hideToastBox(force){
	if (force){
		$('#XG_TOAST_DIV_ID').remove();
		$('#XG_TOAST_MODESCREEN_DIV_ID').remove();
	}
	else{
		$('#XG_TOAST_DIV_ID').hide();
		$('#XG_TOAST_MODESCREEN_DIV_ID').fadeOut();
	}
}

function showMsgBox(mode){
	$('#XG_MSGBOX_DIV_ID').show();
	if (mode == null || mode){
		$('#XG_MODESCREEN_DIV_ID').fadeIn();
	}
}

function getScrollTop(){ 
    var top = 0;
	
    if(document.documentElement && document.documentElement.scrollTop){
        top = document.documentElement.scrollTop;
    }
	else if(document.body){
        top = document.body.scrollTop;
    }

    return top;   
}

function centerWindow(elem, ox, oy){
	var wnd = getCtrl(elem);
	var x = (getClientWidth() - wnd.offsetWidth) / 2;
	var y = (getClientHeight() - wnd.offsetHeight) / 2;
	if (ox) x += ox;
	if (oy) y += oy;
	if (y < 8) y = 8;
	if (y > 200) y -= y / 8;

	wnd.style.left = x + 'px';
	wnd.style.top = getScrollTop() + y + 'px';
}

function setToastTitle(title){
	$('#XG_TOAST_TITLE_LABEL_ID').html(title);
}

function setMessageTitle(title){
	$('#XG_MSGBOX_TITLE_LABEL').html('&nbsp' + title);
}

function setToastErrorText(text, elem){
	var title = $('#XG_TOAST_TITLE_LABEL_ID').html();

	setToastTitle("<font color='#D00'>" + text + "</font>");

	if (elem && strlen(text) > 0){
		$.pack(elem).focus().bind('input propertychange',function(){
			setToastTitle(title);
		});
	}
}

function setMessageErrorText(text, elem){
	$('#XG_MSGBOX_ERRMSG_LABEL').html(text);

	if (elem && strlen(text) > 0){
		$.pack(elem).focus().bind('input propertychange',function(){
			$('#XG_MSGBOX_ERRMSG_LABEL').html('');
		});
	}
}

function showToastMessage(msg, mode, timeout, width, closefunc){
	hideToastBox(true);

	var MsgBoxDiv = createCtrl('div', 'XG_TOAST_DIV_ID');
	
	if (strlen(width) == 0) width = 'auto';

	MsgBoxDiv.style.display = 'none';
	MsgBoxDiv.style.width = width;

	appendCtrl(MsgBoxDiv);

	if (mode == null || mode){
		var ModeScreenDiv = createCtrl('div', 'XG_TOAST_MODESCREEN_DIV_ID');
		
		appendCtrl(ModeScreenDiv);

		if (machinetype == 'MOBILE'){
			ModeScreenDiv.style.background = 'none';
		}

		ModeScreenDiv.style.display = 'none';
		ModeScreenDiv.style.top = getScrollTop() + 'px';

		$('#XG_TOAST_MODESCREEN_DIV_ID').fadeIn();
	}

	msg = "<div id='XG_TOAST_CONTENT_DIV_ID'>" + msg + "</div>";

	if (closefunc){
		$(MsgBoxDiv).html("<span id='XG_TOAST_TITLE_LABEL_ID'></span><span id='XG_TOAST_CLOSE_BUTTON_ID'></span></br>" + msg);

		$('#XG_TOAST_CLOSE_BUTTON_ID').click(function(){
			if (isFunction(closefunc)){
				var flag = closefunc();
				if (flag == null || flag) hideToastBox();
			}
			else{
				hideToastBox();
			}
		});
	}
	else{
		$(MsgBoxDiv).html(msg);
	}

	MsgBoxDiv.style.display = 'block';

   	centerWindow(MsgBoxDiv, 0, 0);

	if (toasthidetimer){
		clearTimeout(toasthidetimer);
		toasthidetimer = null;
	}

	if (timeout && timeout > 0){
		toasthidetimer = setTimeout(hideToastBox, timeout);
	}

	var msgbox = {};

	msgbox['dialog'] = MsgBoxDiv;
	msgbox['timeout'] = timeout;

	msgbox['title'] = function(msg){
		$('#XG_TOAST_TITLE_LABEL_ID').html(msg);
	};

	msgbox['center'] = function(){
		centerWindow(MsgBoxDiv, 0, 0);
	}

	return msgbox;
}

function showToastDialog(data, title, attach){
	var num = 0;
	var max = 4;
	var res = {};
	var msg = data;

	if (isString(data)){
		data = {};
	}
	else{
		if (data.style == null) data.style = [{}];

		while (data.style.length < data.title.length) data.style.push({});

		num = data.title.length;
		msg = "<table class='DialogTable'>"
				+ "<tr v-for='(val,key,idx) in model'><td>"
				+ "<input class='TextLabel' v-model='title[idx]' :size='titlesize' disabled/>"
				+ "<select v-if=\"isArray(style[idx].select)\" v-model='model[key]' :id=\"'vue_toast_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select\" :value='val' v-html='val'></option></select>"
				+ "<select v-else-if=\"strlen(style[idx].select)>0\" v-model='model[key]' :id=\"'vue_toast_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select.split('|')\" :value='val' v-html='val'></option></select>"
				+ "<textarea v-else-if=\"style[idx].type=='textarea'\" v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_toast_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' class='TextArea' style='resize:none'/>"
				+ "<input v-else v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_toast_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' autocomplete='off' class='TextField'/>"
				+ "</td><td v-if='style[idx].minlength' class='MustTagTd'>*</td></tr></table>";

		for (var i = 0; i < num; i++){
			var tmp = getTextSize(data.title[i]);
			if (tmp > max) max = tmp;
		}

		data['titlesize'] = max + 2;
	}

	if (isString(attach)){
		msg += "<div id='XG_TOAST_ATTACH_DIV_ID'>" + attach + '</div>';
	}
	else if (isFunction(attach)){
		msg += "<div id='XG_TOAST_ATTACH_DIV_ID' style='margin-top:10px;padding-top:8px;text-align:center;border-top:1px solid #CCC'><button class='TextButton' style='float:none'>确定</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='TextButton' style='float:none'>取消</button></div>";
	}

	function close(flag){
		if (flag == null) flag = 0;
		for (var i = 0; i < num; i++){
			var elem = getCtrl('vue_toast_dialog_' + i);
			dispatch(elem, 'input');
			if (flag){
				var len = strlen(elem.value);
				if (len > 0 && isFunction(data.style[i].check)){
					if (!data.style[i].check(elem.value)){
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '格式错误' : ' format error'), elem);
						return false;
					}
				}
				if (data.style[i].minlength && len < data.style[i].minlength){
					if (len > 0){
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '长度不足' : ' format error'), elem);
					}
					else{
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '不能为空' : ' required'), elem);
					}
					return false;
				}
			}
		}
		var res = true;
		if (isFunction(attach)){
			res = attach(flag);
			res = res == null || res;
		}
		if (res) hideToastBox(true);
		return res;
    }

    var dialog = showToastMessage(msg, true, null, null, close);

	if (title) dialog.title(title);

	getVue('XG_TOAST_CONTENT_DIV_ID', data);

	res['msgbox'] = dialog;

	dialog.center();

    for (var i = 0; i < num; i++){
		var elem = getCtrl('vue_toast_dialog_' + i);
		if (data.style[i].type == 'textarea') $(elem).height(48).prev().height(48);
		if (data.style[i].setup) data.style[i].setup(elem);
		res[elem.name] = elem;
	}

	if (isFunction(attach)){
		$('#XG_TOAST_ATTACH_DIV_ID button:first').click(function(){
			close(1);
		});

		$('#XG_TOAST_ATTACH_DIV_ID button:last').click(function(){
			close(0);
		});
	}

    return res;
}

function showToast(msg, timeout, width){
	if (timeout == null){
		var len = strlen(msg);
		
		if (len < 10) timeout = 1500;
		else if (len < 20) timeout = 2000;
		else if (len < 30) timeout = 2500;
		else timeout = 3000;
	}

	return showToastMessage(msg, false, timeout, width);
}

function showErrorToast(msg, timeout, width){
	if (timeout == null){
		var len = strlen(msg);
		
		if (len < 10) timeout = 2000;
		else if (len < 20) timeout = 2500;
		else if (len < 30) timeout = 3000;
		else timeout = 3500;
	}
	
	return showToastMessage('<red>' + msg + '</red>', false, timeout, width);
}
function addMsgBoxOption(html){
	return $('#XG_MSGBOX_DIV_CONFIRM_ID').parent().append(html).children().last();
}
function showMessage(msg, title, width, mode, func, attach, data){
	hideMsgBox();
	hideToastBox();

	var MsgBoxDiv = createCtrl('div', 'XG_MSGBOX_DIV_ID');
	
	if (strlen(width) == 0) width = 'auto';

	MsgBoxDiv.style.display = 'none';

	appendCtrl(MsgBoxDiv);

	if (mode == null || mode){
		var ModeScreenDiv = createCtrl('div', 'XG_MODESCREEN_DIV_ID');
		
		appendCtrl(ModeScreenDiv);

		ModeScreenDiv.style.display = 'none';
		ModeScreenDiv.style.top = getScrollTop() + 'px';

		$('#XG_MODESCREEN_DIV_ID').fadeIn();
	}

	var str = "<table><tr><td id='XG_MSGBOX_DIV_TITLE_ID'></td></tr><tr><td><div id='XG_MSGBOX_TEXT_ID'></div></td></tr><tr id='XG_MSGBOX_OPTION_ROW_ID'><td><div style='margin-top:2px;padding-top:6px;text-align:center;border-top:1px solid #888'><button class='TextButton' id = 'XG_MSGBOX_DIV_CONFIRM_ID' onclick='hideMsgBox()'></button>";
	
	if (strlen(attach) > 0) str += attach;

	str += "</div></td></tr></table>";
	
	$(MsgBoxDiv).html(str);

	var MsgLabel = getCtrl('XG_MSGBOX_TEXT_ID');
	var MsgTitle = getCtrl('XG_MSGBOX_DIV_TITLE_ID');
	var ConfirmBtn = getCtrl('XG_MSGBOX_DIV_CONFIRM_ID');

	MsgLabel.style.width = width;

	new Endrag('XG_MSGBOX_DIV_TITLE_ID', 'XG_MSGBOX_DIV_ID');
	
	if (getLanguage() == 'CN'){
		if (title == null) title = '信息';
		ConfirmBtn.innerText = '确认';
	}
	else{
		if (title == null) title = 'MESSAGE';
		ConfirmBtn.innerText = 'CONFIRM';
	}

	$(MsgLabel).html(msg);
	$(MsgTitle).html("<label id='XG_MSGBOX_TITLE_LABEL'>" + title + "</label><label id='XG_MSGBOX_ERRMSG_LABEL'/><label class='ImageButton' id='XG_MSGBOX_CLOSE_ICON'></label>");
	
	var CloseIcon = getCtrl('XG_MSGBOX_CLOSE_ICON');
	var TitleLabel = getCtrl('XG_MSGBOX_TITLE_LABEL');
	
	CloseIcon.onclick = function(){
		if (func == null){
			hideMsgBox();
		}
		else{
			var res = func(0);
			
			if (res == null || res){
				hideMsgBox();
			}
		}
	}

	ConfirmBtn.onclick = function(){
		if (func == null){
			hideMsgBox();
		}
		else{
			var res = func(1);
			
			if (res == null || res){
				hideMsgBox();
			}
		}
	}
	
	if (data == null || isObject(data)){
		getVue('XG_MSGBOX_TEXT_ID', data);
	}

	MsgBoxDiv.style.display = 'block';

   	centerWindow(MsgBoxDiv, 0, 0);

	var msgbox = {};
	
	msgbox['dialog'] = MsgBoxDiv;
	msgbox['confirm'] = ConfirmBtn.onclick;
	
	msgbox['title'] = function(msg){
		$('#XG_MSGBOX_TITLE_LABEL').html(msg);
	};
	
	msgbox['text'] = function(msg){
		$('#XG_MSGBOX_TEXT_ID').html(msg);

		centerWindow(MsgBoxDiv, 0, 0);
	};
	
	msgbox['hideOption'] = function(){
		$('#XG_MSGBOX_OPTION_ROW_ID').hide();
	};
	
	msgbox['showOption'] = function(){
		$('#XG_MSGBOX_OPTION_ROW_ID').show();
	};

	msgbox['setErrorText'] = function(text, elem){
		setMessageErrorText(text, elem);
	};

	msgbox['center'] = function(){
		centerWindow(MsgBoxDiv, 0, 0);
	}

	return msgbox;
}

function showConfirmMessage(msg, title, func, width, mode, data){
	var msgbox = null;
	
	if (getLanguage() == 'CN'){
		msgbox = new showMessage(msg, title, width, mode, func, "<label>&nbsp;&nbsp;</label><button class='TextButton' id='XG_MSGBOX_DIV_CANCEL_ID' onclick='hideMsgBox()'>取消</button>", data);
	}
	else{
		msgbox = new showMessage(msg, title, width, mode, func, "<label>&nbsp;&nbsp;</label><button class='TextButton' id='XG_MSGBOX_DIV_CANCEL_ID' onclick='hideMsgBox()'>CANCEL</button>", data);
	}

	var CancelBtn = getCtrl('XG_MSGBOX_DIV_CANCEL_ID');
	
	CancelBtn.onclick = function(){
		if (func == null){
			hideMsgBox();
		}
		else{
			var res = func(0);
			
			if (res == null || res) hideMsgBox();
		}
	}
	
	msgbox['cancel'] = CancelBtn.onclick;
	
	return msgbox;
}

function showDialog(data, title, func){
	if (isString(data)) return showMessage(data, title, null, true, func);

    if (data.style == null) data.style = [{}];

    while (data.style.length < data.title.length) data.style.push({});

	var max = 4;
	var res = {};
	var num = data.title.length;
    var msg = "<table class='DialogTable'>"
            + "<tr v-for='(val,key,idx) in model'><td>"
            + "<input class='TextLabel' v-model='title[idx]' :size='titlesize' disabled/>"
			+ "<select v-if=\"isArray(style[idx].select)\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select\" :value='val' v-html='val'></option></select>"
            + "<select v-else-if=\"strlen(style[idx].select)>0\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select.split('|')\" :value='val' v-html='val'></option></select>"
            + "<textarea v-else-if=\"style[idx].type=='textarea'\" v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' class='TextArea' style='resize:none'/>"
			+ "<input v-else v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' autocomplete='off' class='TextField'/>"
			+ "</td><td v-if='style[idx].minlength' class='MustTagTd'>*</td></tr></table>";

	for (var i = 0; i < num; i++){
		var tmp = getTextSize(data.title[i]);
		if (tmp > max) max = tmp;
	}

	data['titlesize'] = max + 2;

    res['msgbox'] = showMessage(msg, title, null, true, function(flag){
		for (var i = 0; i < num; i++){
			var elem = getCtrl('vue_dialog_' + i);
			dispatch(elem, 'input');
			if (flag){
				var len = strlen(elem.value);
				if (len > 0 && isFunction(data.style[i].check)){
					if (!data.style[i].check(elem.value)){
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '格式错误' : ' format error'), elem);
						return false;
					}
				}
				if (data.style[i].minlength && len < data.style[i].minlength){
					if (len > 0){
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '长度不足' : ' format error'), elem);
					}
					else{
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '不能为空' : ' required'), elem);
					}
					return false;
				}
			}
		}
        if (func) return func(flag);
    }, null, data);

    for (var i = 0; i < num; i++){
		var elem = getCtrl('vue_dialog_' + i);
		if (data.style[i].type == 'textarea') $(elem).height(48).prev().height(48);
		if (data.style[i].setup) data.style[i].setup(elem);
		res[elem.name] = elem;
	}

    return res;
}

function showConfirmDialog(data, title, func){
	if (isString(data)) return showConfirmMessage(data, title, func);

    if (data.style == null) data.style = [{}];

    while (data.style.length < data.title.length) data.style.push({});

	var max = 4;
	var res = {};
	var num = data.title.length;
    var msg = "<table class='DialogTable'>"
            + "<tr v-for='(val,key,idx) in model'><td>"
            + "<input class='TextLabel' v-model='title[idx]' :size='titlesize' disabled/>"
			+ "<select v-if=\"isArray(style[idx].select)\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select\" :value='val' v-html='val'></option></select>"
            + "<select v-else-if=\"strlen(style[idx].select)>0\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select.split('|')\" :value='val' v-html='val'></option></select>"
            + "<textarea v-else-if=\"style[idx].type=='textarea'\" v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' class='TextArea' style='resize:none' spellcheck='false'/>"
			+ "<input v-else v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' autocomplete='off' spellcheck='false' class='TextField'/>"
			+ "</td><td v-if='style[idx].minlength' class='MustTagTd'>*</td></tr></table>";

	for (var i = 0; i < num; i++){
		var tmp = getTextSize(data.title[i]);
		if (tmp > max) max = tmp;
	}

	data['titlesize'] = max + 2;

    res['msgbox'] = showConfirmMessage(msg, title, function(flag){
		for (var i = 0; i < num; i++){
			var elem = getCtrl('vue_dialog_' + i);
			dispatch(elem, 'input');
			if (flag){
				var len = strlen(elem.value);
				if (len > 0 && isFunction(data.style[i].check)){
					if (!data.style[i].check(elem.value)){
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '格式错误' : ' format error'), elem);
						return false;
					}
				}
				if (data.style[i].minlength && len < data.style[i].minlength){
					if (len > 0){
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '长度不足' : ' format error'), elem);
					}
					else{
						setMessageErrorText(data.title[i] + (lang == 'CN' ? '不能为空' : ' required'), elem);
					}
					return false;
				}
			}
		}
        if (func) return func(flag);
    }, null, true, data);

    for (var i = 0; i < num; i++){
		var elem = getCtrl('vue_dialog_' + i);
		if (data.style[i].type == 'textarea') $(elem).height(48).prev().height(48);
		if (data.style[i].setup) data.style[i].setup(elem);
		res[elem.name] = elem;
	}

    return res;
}

function isTimeout(){
	return strlen(curuser) <= 0;
}

function sessionTimeout(){
	setTimeout(function(){
		showLoginDialog(true);
	}, 10);
}

function getCurrentUser(){
	return curuser;
}

function showNoAccessToast(){
	showErrorToast(lang == 'CN' ? '没有访问权限' : 'permission denied');
}

function setLoginCallback(func){
	logincallback = func;
}

function showLoginDialog(password){
	var msg = getHttpResult('/app/workspace/pub/userlogin.htm');

	showToastMessage(msg, null, null, null, true);
	
	if (password) $('#UserLoginLabel').click();
}

function setCurrentUser(user, updated){
	if (curuser = user){
		if (strlen(user) > 0) setStorage('user', user);

		if (updated){
			if (isFunction(logincallback)){
				logincallback();
			}
			else{
				window.location.reload();
			}
		}
	}
}

function isLeapYear(year){
	if (year % 4) return false;

	if (year % 400 == 0){
		if (year % 3200 == 0) return (year % 153600 == 0) ? true : false;

		return true;
	}

	return (year % 100 == 0) ? false : true;
}

function getMonthDayCount(month, leap){
	var count = -1;

	if (month <= 0 || month > 12) return -1;

	if (month == 2){
		count = leap ? 29 : 28;
	}
	else if (month <= 7){
		count = (month % 2 == 0) ? 30 : 31;
	}
	else{
		count = (month % 2 == 0) ? 31 : 30;
	}

	return count;
}

function getDateCheckCode(year, month, date){
	if (year < -999) return 1;
	if (year > 9999) return 2;
	if (month < 1) return 3;
	if (month > 12) return 4;
	if (date < 1) return 5;

	var days = getMonthDayCount(month, isLeapYear(year));
	
	if (date > days) return 6;

	return 0;
}

function setFrameStyle(top, side, main, bottom, topheight, sidewidth, bottomheight, margin){
	top = getCtrl(top);
	side = getCtrl(side);
	main = getCtrl(main);
	bottom = getCtrl(bottom);
	
	if (margin == null || margin < 0) margin = 0;
	if (topheight == null || topheight < 0) topheight = 0;
	if (sidewidth == null || sidewidth < 0) sidewidth = 0;
	if (bottomheight == null || bottomheight < 0) bottomheight = 0;

	top.style.position = 'absolute';
	top.style.top = margin + 'px';
	top.style.left = margin + 'px';
	top.style.right = margin + 'px';
	top.style.height = (topheight + 1)+ 'px';

	if (topheight > 0){
		top.style.display = 'block';
	}
	else{
		top.style.display = 'none';
	}

	side.style.overflow = 'auto';
	side.style.position = 'absolute';
	side.style.top = (topheight + margin + margin) + 'px';
	side.style.left = top.style.left;
	side.style.width = sidewidth + 'px';
	side.style.bottom = (bottomheight + margin + margin) + 'px';

	if (sidewidth > 0){
		side.style.display = 'block';
	}
	else
	{
		side.style.display = 'none';
	}

	main.style.overflow = 'auto';
	main.style.position = 'absolute';
	main.style.top = side.style.top;
	main.style.left = (sidewidth + margin + margin) + 'px';
	main.style.right = top.style.right;
	main.style.bottom = side.style.bottom;

	bottom.style.position = 'absolute';
	bottom.style.left = top.style.left;
	bottom.style.right = top.style.right;
	bottom.style.bottom = margin + 'px';
	bottom.style.height = bottomheight + 'px';

	if (bottomheight > 0){
		bottom.style.display = 'block';
	}
	else{
		bottom.style.display = 'none';
	}	
}

function DBConnect(name, maxsz){
	if (maxsz == null) maxsz = 1024 * 1024;

	this.db = openDatabase(name, '1.0', name, maxsz);
	
	this.drop = function(tabname){
		this.execute('DROP TABLE ' + tabname);
	};
	
	this.clear = function(tabname){
		this.execute('DELETE FROM ' + tabname);
	};

	this.query = function(sqlcmd, param, callback, errcallback){
		if (isFunction(param)){
			errcallback = callback;
			callback = param;
			param = [];
		}

		this.execute(sqlcmd, param, function(conn, result){
			var list = [];
			for (var i = 0; i < result.rows.length; i++) list[i] = result.rows.item(i);
			callback(list);
		}, errcallback);
	};
	
	this.execute = function(sqlcmd, param, callback, errcallback){
		if (isFunction(param)){
			errcallback = callback;
			callback = param;
			param = [];
		}

		this.db.transaction(function(conn){
			conn.executeSql(sqlcmd, param, callback, errcallback);
		});
	};
}

function loadSimpleExcel(file, callback){
	function load(){
		if (file){
			let reader = new FileReader();
			reader.onload = function(e){
				callback(new SimpleExcel(XLSX.read(e.target.result, {type: 'binary'})));
			};
			reader.readAsBinaryString(file);
		}
	}

	if (typeof(moment) == 'undefined'){
		loadScript('/res/lib/moment.js.gzip', true);
	}

	if (typeof(XLSX) == 'undefined'){
		loadScript('/res/lib/xlsx.js.gzip', true);
		setTimeout(load, 3000);
	}
	else{
		load();
	}
}

(function(){
	vue = Vue;

	getBrowserInfo();

	loadStyle('/res/css/widget.css');

	if (machinetype == 'MOBILE') loadStyle('/res/css/mobile.css');

	$(document).ready(function(){
		if (getBackgroundImage(document.body) == 'none') loadScript('/res/lib/background.js.gzip');
	});

	var checklogintimer = null;

	function checkLogin(){
		getHttpResult('/checklogin', {flag: 'C'}, function(data){
			if (data.code > 0){
				setTimeout(function(){
					setCurrentUser(data.user);
				}, 1000);

				setCurrentUser(data.user);
			}
			else if (data.code == XG_TIMEOUT){
				setCurrentUser(null);
			}
			else if (checklogintimer){
				if (data.status && data.status == 404){
					clearInterval(checklogintimer);
					checklogintimer = null;
				}
			}
		});
	}

	checklogintimer = setInterval(checkLogin, 10000);

	checkLogin();

	window.onresize = function(){
		for (var id in autoresizemap){
			var func = autoresizemap[id];
			if (isFunction(func)) func();
		}
	};

	document.onmouseup = function(){
		var display = $('#XG_TOAST_MODESCREEN_DIV_ID').css('display');
		
		if (display == null || display == 'none') $('#XG_TOAST_DIV_ID').fadeOut();

		if (contextmenuview){
			document.oncontextmenu = defaultcontextmenu;
			$.pack(contextmenuview).hide();
			contextmenuview = null;
		}

		for (var id in automouseupmap){
			var func = automouseupmap[id];
			if (isFunction(func)) func();
		}
	};

	vue.directive('inserted', {
		inserted: function(elem){
			dispatch(elem, 'click');
		}
	});

	vue.component('v-ul', {
		props: ['id', 'title', 'option'],
		template: "<span><ul :id='id'><li v-for=\"text in option.split('|')\" v-html='text'></li></ul></span>"
	});
	
	vue.component('v-ol', {
		props: ['id', 'title', 'option'],
		template: "<span><ol :id='id'><li v-for=\"text in option.split('|')\" v-html='text'></li></ol></span>"
	});
	
	vue.component('v-icon', {
		props: ['id', 'title', 'size', 'path'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><span @click='click'><input :id='id' class='PreviewIcon' style='cursor:default' readonly/></span></span>",
		methods: {
			init : function(){
				if (strlen(this.size) == 0){
					this.size = 15;
				}
				else{
					this.size = parseInt(this.size);
				}

				if (this.size > 0) $('#' + this.id).width(this.size).height(this.size).parent().prev().height(this.size);
			},
			click : function(){
				var pick = new IconPicker(this.id, this.path);
				$('#' + this.id).blur(function(){
					pick.hide();
				});
			}
		}
	});

	vue.component('v-text', {
		props: ['id', 'size', 'type', 'title', 'readonly', 'maxlength', 'placeholder'],
		template: "<span><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><input class='TextField' autocomplete='off' :id='id' :type='type' :size='size' :maxlength='maxlength' :readonly='readonly' :placeholder='placeholder'/></span>"
	});
	
	vue.component('v-label', {
		props: ['id', 'title'],
		template: "<span><input class='TextLabel' type='label' :value='title' :size='getTextSize(title)' :id='id' disabled/></span>"
	});
	
	vue.component('v-border', {
		props: ['size', 'radius', 'color', 'padding'],
		template: "<div :style=\"{padding:padding,display:'inline-block',borderRadius:radius,border:parseInt(size||1) + 'px solid ' + (color||'#999')}\"><slot></slot></div>"
	});
	
	vue.component('v-color', {
		props: ['id', 'title', 'value'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><span @click='click'><input :id='id' size='7' class='TextField' style='cursor:default' readonly/></span></span>",
		methods: {
			init : function(){
			},
			click : function(){
				var pick = new ColorPicker(this.id);
				$('#' + this.id).blur(function(){
					pick.hide();
				});
			}
		}
	});
	
	vue.component('v-button', {
		props: ['id', 'title'],
		template: "<span><input class='TextButton' :value='title' :size='getTextSize(title)' :id='id' onfocus='this.blur()' readonly/></span>"
	});
	
	vue.component('v-select', {
		props: ['id', 'title', 'option'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><select class='TextSelect' :id='id'><option v-for=\"(text, idx) in option.split('|')\" :value='idx' v-html='text'></option></select></span>",
		methods: {
			init : function(){
				setTimeout(function(){
					var widget = $('#' + this.id);
					var height = widget.prev().height();
					widget.height(height).css('line-height', '17px');
				});
			}
		}
	});

	vue.component('v-qrcode', {
		props: ['id', 'size', 'value', 'background', 'foreground'],
		template: "<span @click='init' :id='id' :size='size' :value='value' :background='background' :foreground='foreground' v-inserted></span>",
		methods: {
			init : function(){
				var widget = $('#' + this.id);
				var size = widget.attr('size');
				if (strlen(size) == 0 || (size = parseInt(size)) <= 32) size = 32;
				if (typeof(widget.qrcode) == 'undefined') loadScript('/res/lib/qrcode.js.gzip');
				widget.html('').qrcode({text: widget.val(), width: size, height: size, background: widget.attr('background'), foreground: widget.attr('foreground')});
			}
		}
	});
	
	vue.component('v-textarea', {
		props: ['id', 'title', 'height', 'readonly', 'maxlength', 'placeholder'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><textarea class='TextArea' :id='id' :maxlength='maxlength' :readonly='readonly' :placeholder='placeholder'/></span>",
		methods: {
			init : function(){
				if (strlen(this.height) == 0){
					this.height = 60;
				}
				else{
					this.height = parseInt(this.height);
				}
				$('#' + this.id).height(this.height).css('resize', 'none').prev().height(this.height);
			}
		}
	});

	vue.component('v-password', {
		props: ['id', 'size', 'type', 'title', 'readonly', 'maxlength', 'placeholder'],
		template: "<table class='TextFieldTable' @click='init' v-inserted><tr><td><input type='password' :id='id' :type='type' :size='size' :maxlength='maxlength' :readonly='readonly' :placeholder='placeholder'></td><td><img/></td></tr></table>",
		methods: {
			init : function(){
				if (this.inited) return true;

				var txt = $('#' + this.id);
				var tab = $('#' + this.id).parent().parent().parent();

				if (strlen(this.type) == 0 || this.type.toLowerCase() == 'password'){
					tab.find('img').attr('src', '/res/img/eyeclose.png');
					txt.attr('type', 'password');
				}
				else{
					tab.find('img').attr('src', '/res/img/eyeopen.png');
					txt.attr('type', 'text');
				}

				tab.find('td').css('padding', '0px').css('margin', '0px');
				tab.css('background', txt.css('background'));

				txt.focus(function(){
					tab.addClass('TextFieldTableFocus');
				}).blur(function(){
					tab.removeClass('TextFieldTableFocus');
				});

				tab.find('img').click(function(){
					if (this.src.indexOf('open') > 0){
						this.src = '/res/img/eyeclose.png';
						txt.attr('type', 'password');
					}
					else{
						this.src = '/res/img/eyeopen.png';
						txt.attr('type', 'text');
					}
				});

				this.inited = true;

				return true;
			}
		}
	});

	vue.component('v-datetext', {
		props: ['id', 'min', 'max', 'title', 'readonly', 'placeholder'],
		template: "<span><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled='disabled'/><input class='TextField' @click='init' type='text' :id='id' :size='10' :readonly='readonly' :placeholder='placeholder'/></span>",
		methods: {
			init: function(){
				var dt = getDateTime().substr(0, 10);

				if ((strlen(this.min) == 0 || dt >= this.min) && (strlen(this.max) == 0 || dt <= this.max)){
					dt = true;
				}
				else{
					dt = false;
				}
				
				laydate({
					elem: '#' + this.id,
					istoday: dt,
					istime: false,
					min: this.min,
					max: this.max,
					format: 'YYYY-MM-DD',
				});
			}
		}
	});
	
	vue.component('v-datetimetext', {
		props: ['id', 'min', 'max', 'title', 'readonly', 'placeholder'],
		template: "<span><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled='disabled'/><input class='TextField' @click='init' type='text' :id='id' :size='19' :readonly='readonly' :placeholder='placeholder'/></span>",
		methods: {
			init: function(){
				var dt = getDateTime();
				
				if ((strlen(this.min) == 0 || dt >= this.min) && (strlen(this.max) == 0 || dt <= this.max)){
					dt = true;
				}
				else{
					dt = false;
				}
				
				laydate({
					elem: '#' + this.id,
					istoday: true,
					istime: true,
					min: this.min,
					max: this.max,
					format: 'YYYY-MM-DD hh:mm:ss',
				});
			}
		}
	});

	vue.component('v-labelwidget', {
		props: ['id', 'title', 'value'],
		template: "<table @click='init' v-inserted><tr><td style='vertical-align:top'><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/></td><td><div style='block' :id='id'/></td></tr></table>",
		methods: {
			init: function(){
				if (!isFunction(this['val'])){
					var div = $.pack(this.id);
					var elem = new LabelWidget(div, this.value);
					this['val'] = elem.val;
					div['val'] = elem.val;
				}
			}
		}
	});
}());