function BarChart(elem, title, xaxis, series){
	elem = getCtrl(elem);

	var data = [];
	var group = [];
	var color = [];
	var chart = echarts.init(elem);
	var colorlist = ['#91C7AE', '#D48265', '#61A0A8', '#2F4554', '#D53A35'];

	for (var i = 0; i < series.length; i++){
		var arr = [];
		var item = series[i];
		var list = item.list;
		group.push(item.group);
		color.push(item.color ? item.color : colorlist[i]);
		for (var j = 0; j < list.length; j++) arr.push(list[j]);
		data.push({data: arr, type: 'bar', name: item.group, itemStyle: {normal: {color: item.color}}});
	}

	var option = {
		title: {x: 'center', text: title},
		legend: {data: group, top: 'bottom'},
        xAxis: {data: xaxis},
        yAxis: {},
        series: data,
		tooltip: {
			padding: [0],
			trigger: 'item',
			borderRadius: '0px',
			backgroundColor: 'none',
			formatter: function(data){
				return '<tips>' + data.value + '</tips>';
			}
		}
	}

	chart.setOption(option);

	this.option = option;
	this.color = color;
	this.chart = chart;
	this.elem = elem;
}