function GaugeChart(elem, title, maxval, minval, label){
	elem = getCtrl(elem);

	var fmt = '{value}';
	var chart = echarts.init(elem);
	var width = getWidth(elem) / 12;

	if (width > 30) width = 30;
	if (width < 10) width = 10;
	if (minval == null) minval = 0;
	if (maxval == null) maxval = minval + 100;
	if (minval == 0 && maxval == 100) fmt += '%';

	var option = {
		title: {x: 'center', text: title},
		series: [{
			min: minval,
			max: maxval,
			type: 'gauge',
			detail: {formatter: fmt},
			data:[{value: minval, name: label}],
			axisLine: {lineStyle: {width: width}}
		}]
	}

	var data = option.series[0].data[0];

	chart.setOption(option);

	this.getValue = function(){
		return data.value;
	}

	this.setValue = function(val){
		if (val != data.value){
			data.value = val;
			chart.setOption(option, true);
		}
	}

	this.option = option;
	this.chart = chart;
	this.elem = elem;
}