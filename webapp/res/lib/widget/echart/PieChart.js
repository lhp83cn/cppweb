function PieChart(elem, title, series){
	elem = getCtrl(elem);

	var group = [];
	var chart = echarts.init(elem);
	var label = {
		formatter: '{title|{b}\n{d}%}',
		rich: {
			title: {
				align: 'center',
				padding: [2, 0]
			}
		}
	};

	for (var i = 0; i < series.length; i++){
		var item = series[i];
		group.push(item.name);
		if (item.label == null) item.label = label;
	}

	chart.setOption({
		title: {x: 'center', text: title},
		legend: {data: group, top: 'bottom'},
		series: [{type: 'pie', radius: '55%', data: series}],
		emphasis: {
            itemStyle: {
            	shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        },
		tooltip: {
			padding: [0],
			trigger: 'item',
			borderRadius: '0px',
			backgroundColor: 'none',
			formatter: function(data){
				return '<tips>' + data.value + '</tips>';
			}
		}
	});

	this.chart = chart;
	this.elem = elem;
}