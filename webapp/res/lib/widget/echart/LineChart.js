function LineChart(elem, title, xaxis, series){
	elem = getCtrl(elem);

	var data = [];
	var group = [];
	var color = [];
	var chart = echarts.init(elem);
	var colorlist = ['#91C7AE', '#D48265', '#61A0A8', '#2F4554', '#D53A35'];

	for (var i = 0; i < series.length; i++){
		var arr = [];
		var item = series[i];
		var list = item.list;
		group.push(item.group);
		color.push(item.color ? item.color : colorlist[i]);
		for (var j = 0; j < list.length; j++) arr.push(list[j]);
		data.push({data: arr, type: 'line', name: item.group, itemStyle: {normal: {color: item.color}}});
	}

	var option = {
		title: {x: 'center', text: title},
		legend: {data: group, top: 'bottom'},
        xAxis: {data: xaxis, boundaryGap: false},
        yAxis: {},
        series: data,
		tooltip: {
			padding: [0],
			trigger: 'item',
			borderRadius: '0px',
			backgroundColor: 'none',
			formatter: function(data){
				return '<tips>' + data.value + '</tips>';
			}
		}
	}

	chart.setOption(option);

	this.min = function(val){
		option.yAxis['min'] = val;
		chart.setOption(option);
	}

	this.max = function(val){
		option.yAxis['max'] = val;
		chart.setOption(option);
	}

	this.scale = function(flag){
		option.yAxis['scale'] = flag;
		chart.setOption(option);
	}

	this.symbol = function(size){
		var vec = option.series;
		for (var i = 0; i < vec.length; i++){
			vec[i]['symbolSize'] = size;
		}
		chart.setOption(option);
	}

	this.smooth = function(flag){
		var vec = option.series;
		for (var i = 0; i < vec.length; i++){
			vec[i]['smooth'] = flag;
		}
		chart.setOption(option);
	}

	this.interval = function(gap){
		option.xAxis['axisLabel'] = {
			interval: gap
		}
		chart.setOption(option);
	}

	this.push = function(a, b, c, d, e){
		function push(idx, val){
			var vec = option.series[idx]['data'];
			if (vec.length > 0){
				if (option.series[idx]['symbolSize'] === 0){
					var item = vec[vec.length - 1];
					if (isObject(item)) item['symbolSize'] = 0;
				}
				vec.pop_front();
			}
			vec.push(val);
		}

		if (a || a == 0) push(0, a);
		if (b || b == 0) push(1, b);
		if (c || c == 0) push(2, c);
		if (d || d == 0) push(3, d);
		if (e || e == 0) push(4, d);

		chart.setOption(option);
	}

	this.option = option;
	this.color = color;
	this.chart = chart;
	this.elem = elem;
}