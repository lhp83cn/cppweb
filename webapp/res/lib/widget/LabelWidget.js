function LabelWidget(elem, value, filter){
	elem = $.pack(elem);

	var self = this;
	var labels = [];
	var imgcss = "style='width:8px;height:8px;margin-left:4px;display:inline-block;cursor:pointer;'";
	var spancss = "style='margin:0px 2px 4px 2px;padding:4px;border-radius:2px;display:inline-block;border:1px solid #AAA;'";

	if (filter == null) filter = isFileName;

	function addItem(){
		let data = {
			title: ['标签'],
			model: {label: ''},
			style: [{size: 16, minlength: 1, maxlength: 64}]
		};

		let dialog = showToastDialog(data, '添加标签', function(flag){
			if (flag){
				if (!self.add(data.model.label)){
					showToast('<red>相同标签已存在</red>');
				}
			}
		});

		$.pack(dialog.label).change(function(){
			if (!filter($(this).val())){
				$(this).val('');
			}
		});
	}

	elem.append("<span " + spancss + "><red style='padding:0px 4px;cursor:pointer'>+</red></span>").children('span').click(function(){
		addItem(this);
	});

	this.add = function(text){
		for (let i = 0; i < labels.length; i++){
			if (labels[i] == text) return false;
		}
		var msg = "<span " + spancss + "><green>" + text + "</green><img " + imgcss + " src='/res/img/cross.png'></img></span>";
		elem.children('span:last').before(msg).prev().children('img').click(function(){
			$.pack(this).parent().remove();
		});
		labels.push(text);
		return true;
	}

	this.val = function(value){
		if (value == null) return labels.join(',');
		if (value == '') return labels = [];
		let arr = value.split(',');
		for (let i = 0; i < arr.length; i++){
			self.add(arr[i]);
		}
	}

	this.val(value);
}