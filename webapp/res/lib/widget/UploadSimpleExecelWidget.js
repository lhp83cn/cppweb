function UploadSimpleExecelWidget(button, title, fieldmap, callback, pagesize, startindex){
	let last = null;
	var self = this;
	let elem = new UploadFileWidget(button, title, null, 4 * 1024 * 1024);

	if (startindex == null) startindex = 0;

	self.file = null;
	self.list = null;
	self.xlsx = null;
	self.label = elem.label;
	self.setFilter = elem.setFilter;
	self.upload = function(callback){
		if (callback){
			let list = self.list;
			if (list == null || list.length <= 0){
				showToast('请选择要导入的文件');
				return false;
			}
			if (last == list){
				showToast('不要重复导入');
				return false;
			}
			let uploadnum = 0;
			let uploadsize = pagesize;
			if (uploadsize == null){
				uploadsize = list.length / 3;
				if (uploadsize < 10) uploadsize = 10;
				if (uploadsize > 100) uploadsize = 100;
			}
			showToastMessage('正在上传数据...');
			function upload(){
				let sublist = list.slice(uploadnum, uploadnum + uploadsize);
				let res = callback(sublist, list.length <= uploadnum + sublist.length);
				if (res == null || res >= 0){
					uploadnum += sublist.length;
					if (uploadnum < list.length){
						showToast('当前导入进度：<green>' + uploadnum + ' / ' + list.length + '</green>');
						setTimeout(upload, 10);
					}
					else{
						showToast('成功导入<green>' + uploadnum + '</green>条记录');
					}
				}
				else{
					if (uploadnum == 0 && res == XG_TIMEOUT){
						sessionTimeout();
					}
					else{
						let errnum = list.length - uploadnum;
						showMessage(uploadnum > 0 ? '处理成功：<green>' + uploadnum + '</green>条，处理失败：<red>' + errnum + '</red>条' : '<red>数据导入失败</red>', '导入异常');
					}
					last = null;
				}
			}
			setTimeout(upload, 10);
			last = list;
		}
	}

	elem.setFilter('.xls,.xlsx').change(function(file){
		showToastMessage('正在解析导入文件...');
		loadSimpleExcel(file, function(xlsx){
			self.file = file;
			self.xlsx = xlsx;
			self.list = xlsx.toArray(fieldmap);
			if (self.list.length <= startindex || xlsx.cols() < Object.keys(fieldmap).length){
				showToast('文件数据错误');
			}
			else{
				self.list = self.list.slice(startindex);
				if (callback){
					showConfirmMessage('表格共<b>' + self.list.length + '</b>行<b>' + xlsx.cols() + '</b>列，是否马上导入？', '导入选项',function(flag){
						if (flag) self.upload(callback);
					});
				}
				else{
					hideToastBox();
				}
				elem.label.val(file.name);
			}
		});
	});
}