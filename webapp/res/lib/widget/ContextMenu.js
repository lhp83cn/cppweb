function ContextMenu(menus, click, parent){
	if (isString(menus)) menus = menus.split(',');

	parent = parent ? $.pack(parent) : $(document.body);

	var view = parent.append("<div class='ContextMenuDialog'></div>").children('div:last');

	menus.forEach(function(item){
		view.append('<div>' + item + '</div>');
	});

	view.children('div').click(function(){
		click($(this).text(), getContextMenuAttach(), this);
	});

	this.view = view;

	this.bind = function(elem){
		bindContextMenu(elem, view);
	}
}