function AlbumBox(elem, img, url, timer){
	elem = $.pack(elem);

	var str = '';
	var page = 0;
	var id = 'AlbumBox' + getSequence();
	var html = "<table id='" + id + "'><tr><td style='height:16px'></td></tr><tr><td align='left'>";
	
	for (var i = 0; i < img.length; i++){
		str = str + "<span class='Point" + id + "'></span>";
	}
	
	html += "<img id='LastPage" + id + "' src='/res/img/left.png' /></td><td align='right'>"
	html += "<img id='NextPage" + id + "' src='/res/img/right.png' /></td></tr>"
	html += "<tr><td colspan='2' style='height:16px;text-align:center;vertical-align:middle'>" + str + "</td></tr></table>";
	
	elem.append(html);
	
	var widget = $('.Point' + id);
	
	widget.css('width', '12px').css('height', '12px').css('display', 'inline-block');
	widget.css('border-radius', '8px').css('margin', '2px 8px');

	widget = $('#' + id);
	
	var showImage = function(idx){
		if (idx == null) idx = 0;
		else if (idx >= img.length) idx = 0;
		else if (idx < 0) idx = img.length - 1;
		
		page = idx;
		widget.hide();
		widget.css('width', '100%').css('height', '100%');
		widget.css('background-image', 'url(' + img[page] + ')');
		widget.css('background-position', 'center');
		widget.css('background-repeat', 'no-repeat');
		widget.css('background-size', '100% 100%');
		widget.fadeIn();
		
		$('.Point' + id).css('background', '#888');
		$('.Point' + id + ':eq(' + page + ')').css('background', '#E00');
	}
	
	$('.Point' + id).css('background', '#888').click(function(e){
		e.stopPropagation();
		showImage($(this).index());
	}).hover(function(){
		if ($(this).index() != page){
			$(this).css('background', '#888');
		}
	}).mouseup(function(){
		if ($(this).index() != page){
			$(this).css('background', '#888');
		}
	}).mouseout(function(){
		if ($(this).index() != page){
			$(this).css('background', '#888');
		}
	}).mousedown(function(){
		if ($(this).index() != page){
			$(this).css('background', '#888');
		}
	});
	
	var getIndex = function() {
		return page;
	}

	var lastImage = function(){
		showImage(page - 1);
	}
	
	var nextImage = function(){
		showImage(page + 1);
	}
	
	widget.click(function(){
		if (url && url[page]){
			window.open(url[page]);
		}
	});

	widget.css('#NextPage', 'background-image 0.5s ease-in-out');
	
	$('#LastPage' + id).css('opacity', '0.2').click(function(event){
		event.stopPropagation();
		lastImage();
	}).mousedown(function(){
		$(this).css('opacity', '0.2');
	}).mouseup(function(){
		$(this).css('opacity', '0.8');
	});
	
	$('#NextPage' + id).css('opacity', '0.2').click(function(event){
		event.stopPropagation();
		nextImage();
	}).mousedown(function(){
		$(this).css('opacity', '0.3');
	}).mouseup(function(){
		$(this).css('opacity', '0.8');
	});
	
	widget.hover(function(){
		$('#LastPage' + id).css('opacity', '0.8');
		$('#NextPage' + id).css('opacity', '0.8');
	}).mouseup(function(){
		$('#LastPage' + id).css('opacity', '0.8');
		$('#NextPage' + id).css('opacity', '0.8');
	}).mouseleave(function(){
		$('#LastPage' + id).css('opacity', '0.3');
		$('#NextPage' + id).css('opacity', '0.2');
	});
	
	this.getIndex = getIndex;
	this.showImage = showImage;
	this.nextImage = nextImage;
	this.lastImage = lastImage;
	this.getWidget = function(){
		return widget;
	}

	showImage(0);

	if (timer) {
		var idx = 0;
		var num = getIndex();
		var second = timer / 1000;

		setInterval(function(){
			if (num == getIndex()) {
				if (++idx >= second) {
					idx = 0;
					nextImage();
					num = getIndex();
				}
			}
			else {
				idx = 0;
				num = getIndex();
			}
		}, 1000)
	}
}