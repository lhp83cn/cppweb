#ifndef XG_WEBX_ROUTE_H
#define XG_WEBX_ROUTE_H
///////////////////////////////////////////////////////////
#include "std.h"

namespace webx
{
	class AccessItem
	{
	protected:
		int access;
		bool remote;
		string param;
		set<string> group;
		HttpDataNode node;

	public:
		int getWeight() const
		{
			return node.size();
		}
		bool isRemote() const
		{
			return remote;
		}
		string getGroupString() const
		{
			string res;
	
			if (group.empty()) return res;

			for (const string& item : group) res += "," + item;

			return res.substr(1);
		}
		string getParamString() const
		{
			return param;
		}

		int check(const string& param, const vector<string>& grouplist) const;
		AccessItem(const string& param, const vector<string>& groupset, bool remote = false);
	};

	bool NeedUpdateRouteHost();
	void InitNewUser(const string& user);
	string GetParameter(const string& id);
	sp<DBConnect> GetDBConnect(const string& dbid = "");
	bool LoadAccessMap(map<string, vector<AccessItem>>& accessmap);
	void SetCgiDefaultGroup(const string& path, const string& grouplist);

	HostItem GetLogHost();
	int GetLastRemoteStatus();
	HostItem GetRegCenterHost();
	void CheckSystemRight(ProcessBase* proc);
	HostItem GetRouteHost(const string& path);
	int SetLogHost(const string& host, int port);
	int UpdateRouteList(const string& host, int port);
	int CheckAccess(const IHttpRequest* request, const string& grouplist = "");
	int CheckAccess(const string& path, const string& param, const string& grouplist = "");
	int Broadcast(const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	int NotifyHost(const string& host, int port, const string& path, const string& param = "", const string& contype = "", const string& cookie = "");


	int GetRemoteProcessTimeout(const string& path);
	void SetRemoteProcessTimeout(const string& path, int timeout);
	SmartBuffer GetRemoteResult(const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	void GetRemoteResult(const string& path, JsonReflect& response, const Object& param = JsonReflect(), const string& contype = "", const string& cookie = "");

	void RemoveConfileCache(const string& name);
	sp<YAMLoader> GetConfile(const string& name);
	string GetConfig(const string& name, const string& key = "");
	int SyncRemoteConfig(const string& type, const string& name);
	string GetRemoteConfig(const string& type, const string& name);
};

///////////////////////////////////////////////////////////
#endif
