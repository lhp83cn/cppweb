#ifndef XG_ENTITY_CT_XG_FILE_H
#define XG_ENTITY_CT_XG_FILE_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_FILE : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	id;
	DBString	rid;
	DBString	type;
	DBString	imme;
	DBString	remark;
	DBBlob		content;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_FILE";
	}

	static const char* GetColumnString()
	{
		return "ID,RID,TYPE,IMME,REMARK,CONTENT,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
