#ifndef XG_ENTITY_CT_XG_TABETC_H
#define XG_ENTITY_CT_XG_TABETC_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_TABETC : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	id;
	DBString	dbid;
	DBString	name;
	DBString	title;
	DBString	remark;
	DBInteger	enabled;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_TABETC";
	}

	static const char* GetColumnString()
	{
		return "ID,DBID,NAME,TITLE,REMARK,ENABLED,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
