#include "T_XG_USER.h"


void CT_XG_USER::clear()
{
	this->user.clear();
	this->dbid.clear();
	this->icon.clear();
	this->name.clear();
	this->password.clear();
	this->menulist.clear();
	this->grouplist.clear();
	this->language.clear();
	this->level.clear();
	this->enabled.clear();
	this->certno.clear();
	this->mail.clear();
	this->phone.clear();
	this->address.clear();
	this->remark.clear();
	this->statetime.clear();
}
int CT_XG_USER::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_USER(" + string(GetColumnString()) + ") VALUES(";
	sql += this->user.toValueString(conn->getSystemName());
	vec.push_back(&this->user);
	sql += ",";
	sql += this->dbid.toValueString(conn->getSystemName());
	vec.push_back(&this->dbid);
	sql += ",";
	sql += this->icon.toValueString(conn->getSystemName());
	vec.push_back(&this->icon);
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->password.toValueString(conn->getSystemName());
	vec.push_back(&this->password);
	sql += ",";
	sql += this->menulist.toValueString(conn->getSystemName());
	vec.push_back(&this->menulist);
	sql += ",";
	sql += this->grouplist.toValueString(conn->getSystemName());
	vec.push_back(&this->grouplist);
	sql += ",";
	sql += this->language.toValueString(conn->getSystemName());
	vec.push_back(&this->language);
	sql += ",";
	sql += this->level.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->certno.toValueString(conn->getSystemName());
	vec.push_back(&this->certno);
	sql += ",";
	sql += this->mail.toValueString(conn->getSystemName());
	vec.push_back(&this->mail);
	sql += ",";
	sql += this->phone.toValueString(conn->getSystemName());
	vec.push_back(&this->phone);
	sql += ",";
	sql += this->address.toValueString(conn->getSystemName());
	vec.push_back(&this->address);
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_USER::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->user = row->getString(0);
	this->user.setNullFlag(row->isNull());
	this->dbid = row->getString(1);
	this->dbid.setNullFlag(row->isNull());
	this->icon = row->getString(2);
	this->icon.setNullFlag(row->isNull());
	this->name = row->getString(3);
	this->name.setNullFlag(row->isNull());
	this->password = row->getString(4);
	this->password.setNullFlag(row->isNull());
	this->menulist = row->getString(5);
	this->menulist.setNullFlag(row->isNull());
	this->grouplist = row->getString(6);
	this->grouplist.setNullFlag(row->isNull());
	this->language = row->getString(7);
	this->language.setNullFlag(row->isNull());
	this->level = row->getLong(8);
	this->level.setNullFlag(row->isNull());
	this->enabled = row->getLong(9);
	this->enabled.setNullFlag(row->isNull());
	this->certno = row->getString(10);
	this->certno.setNullFlag(row->isNull());
	this->mail = row->getString(11);
	this->mail.setNullFlag(row->isNull());
	this->phone = row->getString(12);
	this->phone.setNullFlag(row->isNull());
	this->address = row->getString(13);
	this->address.setNullFlag(row->isNull());
	this->remark = row->getString(14);
	this->remark.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(15);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_USER::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_USER";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_USER::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->user);
	return find(getPKCondition(), vec);
}
string CT_XG_USER::getPKCondition()
{
	string condition;
	condition = "USER=";
	condition += this->user.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_USER::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_USER SET ";
	if (updatenull || !this->dbid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "DBID=";
		sql += this->dbid.toValueString(conn->getSystemName());
		v.push_back(&this->dbid);
	}
	if (updatenull || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->password.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PASSWORD=";
		sql += this->password.toValueString(conn->getSystemName());
		v.push_back(&this->password);
	}
	if (updatenull || !this->menulist.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MENULIST=";
		sql += this->menulist.toValueString(conn->getSystemName());
		v.push_back(&this->menulist);
	}
	if (updatenull || !this->grouplist.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "GROUPLIST=";
		sql += this->grouplist.toValueString(conn->getSystemName());
		v.push_back(&this->grouplist);
	}
	if (updatenull || !this->language.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "LANGUAGE=";
		sql += this->language.toValueString(conn->getSystemName());
		v.push_back(&this->language);
	}
	if (updatenull || !this->level.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "LEVEL=";
		sql += this->level.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->certno.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CERTNO=";
		sql += this->certno.toValueString(conn->getSystemName());
		v.push_back(&this->certno);
	}
	if (updatenull || !this->mail.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MAIL=";
		sql += this->mail.toValueString(conn->getSystemName());
		v.push_back(&this->mail);
	}
	if (updatenull || !this->phone.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PHONE=";
		sql += this->phone.toValueString(conn->getSystemName());
		v.push_back(&this->phone);
	}
	if (updatenull || !this->address.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ADDRESS=";
		sql += this->address.toValueString(conn->getSystemName());
		v.push_back(&this->address);
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->user);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_USER::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_USER";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_USER::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->user);
	return remove(getPKCondition(), vec);
}
string CT_XG_USER::getValue(const string& key)
{
	if (key == "USER") return this->user.toString();
	if (key == "DBID") return this->dbid.toString();
	if (key == "ICON") return this->icon.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "PASSWORD") return this->password.toString();
	if (key == "MENULIST") return this->menulist.toString();
	if (key == "GROUPLIST") return this->grouplist.toString();
	if (key == "LANGUAGE") return this->language.toString();
	if (key == "LEVEL") return this->level.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "CERTNO") return this->certno.toString();
	if (key == "MAIL") return this->mail.toString();
	if (key == "PHONE") return this->phone.toString();
	if (key == "ADDRESS") return this->address.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_USER::setValue(const string& key, const string& val)
{
	if (key == "USER")
	{
		this->user = val;
		return true;
	}
	if (key == "DBID")
	{
		this->dbid = val;
		return true;
	}
	if (key == "ICON")
	{
		this->icon = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "PASSWORD")
	{
		this->password = val;
		return true;
	}
	if (key == "MENULIST")
	{
		this->menulist = val;
		return true;
	}
	if (key == "GROUPLIST")
	{
		this->grouplist = val;
		return true;
	}
	if (key == "LANGUAGE")
	{
		this->language = val;
		return true;
	}
	if (key == "LEVEL")
	{
		this->level = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "CERTNO")
	{
		this->certno = val;
		return true;
	}
	if (key == "MAIL")
	{
		this->mail = val;
		return true;
	}
	if (key == "PHONE")
	{
		this->phone = val;
		return true;
	}
	if (key == "ADDRESS")
	{
		this->address = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_USER::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_USER SET ";
	if (updatenull || !this->dbid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "DBID=";
		sql += this->dbid.toValueString(conn->getSystemName());
		v.push_back(&this->dbid);
	}
	if (updatenull || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->password.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PASSWORD=";
		sql += this->password.toValueString(conn->getSystemName());
		v.push_back(&this->password);
	}
	if (updatenull || !this->menulist.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MENULIST=";
		sql += this->menulist.toValueString(conn->getSystemName());
		v.push_back(&this->menulist);
	}
	if (updatenull || !this->grouplist.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "GROUPLIST=";
		sql += this->grouplist.toValueString(conn->getSystemName());
		v.push_back(&this->grouplist);
	}
	if (updatenull || !this->language.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "LANGUAGE=";
		sql += this->language.toValueString(conn->getSystemName());
		v.push_back(&this->language);
	}
	if (updatenull || !this->level.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "LEVEL=";
		sql += this->level.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->certno.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CERTNO=";
		sql += this->certno.toValueString(conn->getSystemName());
		v.push_back(&this->certno);
	}
	if (updatenull || !this->mail.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MAIL=";
		sql += this->mail.toValueString(conn->getSystemName());
		v.push_back(&this->mail);
	}
	if (updatenull || !this->phone.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PHONE=";
		sql += this->phone.toValueString(conn->getSystemName());
		v.push_back(&this->phone);
	}
	if (updatenull || !this->address.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ADDRESS=";
		sql += this->address.toValueString(conn->getSystemName());
		v.push_back(&this->address);
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
