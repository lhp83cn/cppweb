#ifndef XG_ENTITY_CT_XG_TABCOLS_H
#define XG_ENTITY_CT_XG_TABCOLS_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_TABCOLS : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	name;
	DBString	tabid;
	DBString	title;
	DBString	filter;
	DBString	remark;
	DBInteger	enabled;
	DBInteger	position;
	DBInteger	querytype;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_TABCOLS";
	}

	static const char* GetColumnString()
	{
		return "NAME,TABID,TITLE,FILTER,REMARK,ENABLED,POSITION,QUERYTYPE,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
