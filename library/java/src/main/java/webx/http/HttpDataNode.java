package webx.http;

import java.util.Map;

public class HttpDataNode extends HttpHeadNode{
	public String toString(){
		String res = "";

		if (map.isEmpty()) return res;

		for (Map.Entry<String, String> item : map.entrySet()){
			res += "&" + item.getKey() + "=" + Http.Encode(item.getValue());
		}

		return res.substring(1);
	}
	public int parse(String msg){
		String[] vec = msg.split("&");
		
		map.clear();

		for (String item : vec){
			String[] arr = item.split("=");
			if (arr.length < 2) continue;

			String key = arr[0].trim();
			if (key.isEmpty()) continue;

			String tmp = map.get(key);
			String val = Http.Decode(arr[1]);

			if (tmp == null){
				map.put(key, val);
			}
			else{
				map.put(key, tmp + val);
			}
		}

		return map.size();
	}
	public String get(String key){
		return Http.Decode(map.get(key));
	}
	public void set(String key, String val){
		set(key, Http.Encode(val), false);
	}
}