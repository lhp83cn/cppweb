package webx;

import stdx.Utils;
import stdx.Optional;
import stdx.Required;
import stdx.BaseException;
import webx.json.JsonObject;

import webx.http.Http;
import webx.http.HttpRequest;
import webx.http.HttpResponse;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Type;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

public class WebApp{
	public native static int GetPort();
	public native static String GetId();
	public native static String GetHost();
	public native static String GetPath();
	public native static String GetSequence();
	public native static int GetStartStatus();
	public native static void Loop(String path);
	public native static void SetLogFlag(int flag);
	public native static void SetClassPath(String path);
	public native static String GetMimeType(String key);
	public native static String GetParameter(String key);
	public native static String GetRouteHost(String path);
	public native static void Trace(int level, String msg);
	public native static String GetConfig(String name, String key);
	public native static String GetWebAppPath(String path, String filename);
	public native static int CheckLimit(String key, int maxtimes, int timeout);
	public native static int CheckAccess(String path, String param, String grouplist);

	public native static void SetCgiAccess(String path, String access);
	public native static void SetCgiExtdata(String path, String extdata);
	public native static void SetCgiDefaultGroup(String path, String grouplist);
	public native static void SetCgiDoc(String path, String reqdoc, String rspdoc, String remark);

	public native static int DisableSession(String sid);
	public native static String GetSession(String sid, String key);
	public native static int CreateSession(String sid, int timeout);
	public native static int SetSession(String sid, String key, String val);

	public native static int GetLastRemoteStatus();
	public native static String GetRemoteResult(String path, String param, String contype, String cookie);

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Path{
		String value();
		String group() default "";
		String access() default "private";
	}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface TimerTask{
		int value();
	}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface DailyTask{
		String value();
	}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Document{
		Class request();
		Class response();
		String remark();
	}

	String sid = null;
	HttpRequest request = null;
	HttpResponse response = null;

	public byte[] process(byte[] msg){
		byte[] res = null;

		request = new HttpRequest(msg);
		response = new HttpResponse();

		try{
			process(request, response);
		}
		catch(BaseException e){
			if (e.getErrorCode() == BaseException.NOTCHANGED.getErrorCode()){
				String etag = request.getHeader("If-None-Match");
				if (Utils.IsNotEmpty(etag)){
					response.setHeader("ETag", etag);
					response.setStatus(304);

					return response.getBytes();
				}
			}

			LogFile.Error(e);

			response.setStatus(GetHttpStatus(e.getErrorCode()));

			JsonObject json = new JsonObject("{}");
			Map<String, Object> map = e.getFields();
			if (Utils.IsNotEmpty(map)){
				JsonObject extra = json.addObject("extra");
				for (Map.Entry<String, Object> item : map.entrySet()){
					Object val = item.getValue();
					if (val instanceof JsonObject){
						extra.put(item.getKey(), (JsonObject)(val));
					}
					else{
						extra.put(item.getKey(), String.valueOf(val));
					}
				}
			}

			json.put("code", e.getErrorCode());
			json.put("desc", e.getMessage());

			response.setBody(json.toString());
		}
		catch(Throwable e){
			LogFile.Error(e);

			response.setStatus(GetHttpStatus(BaseException.SYSERR.getErrorCode()));

			JsonObject json = new JsonObject("{}");

			json.put("code", BaseException.SYSERR.getErrorCode());
			json.put("desc", BaseException.SYSERR.getMessage());

			response.setBody(json.toString());
		}

		return response.getBytes();
	}
	public String checkLogin() throws Exception{
		return checkLogin(null);
	}
	public String checkLogin(JsonObject data) throws Exception{
		sid = request.getSessionId();

		if (Utils.IsEmpty(sid)) throw BaseException.TIMEOUT;

		String msg = GetRemoteResult("CheckLogin", "flag=C&sid=" + sid, null, request.getHeader("Cookie"));

		if (Utils.IsEmpty(msg)) throw BaseException.TIMEOUT;

		if (data == null){
			data = new JsonObject(msg);
		}
		else{
			data.parse(msg);
		}

		if (!data.has("code")) throw BaseException.SYSERR;
			
		int code = data.asInt("code");
		String desc = data.asString("desc");

		if (code == BaseException.AUTHFAIL.getErrorCode()) throw BaseException.AUTHFAIL;
		if (code == BaseException.TIMEOUT.getErrorCode()) throw BaseException.TIMEOUT;
		if (code < 0) throw BaseException.SYSERR;

		checkAccess(data.asString("grouplist"));

		return data.asString("user");
	}
	public void checkAccess(String grouplist) throws Exception{
		String param = new String(request.getBody(), Http.GetCharset());
		if (CheckAccess(request.getPath(), param, grouplist) < 0) throw BaseException.AUTHFAIL;
	}
	public void process(HttpRequest request, HttpResponse response) throws Exception{
	}

	public static String GetClassPath(){
		String path = System.getProperty("java.class.path");

		if (Utils.IsNotEmpty(path)) return path;
		
		return GetResourceRootPath();
	}
	public static String GetPluginPath(){
		return Utils.IsLinux() ? "/opt/cppweb/webapp/etc/plugin/bin" : "c:/cppweb/webapp/etc/plugin/bin";
	}
	public static int GetHttpStatus(int code){
		if (code == BaseException.TIMEOUT.getErrorCode()) return 401;
		if (code == BaseException.SYSBUSY.getErrorCode()) return 503;
		if (code == BaseException.DAYLIMIT.getErrorCode()) return 503;
		if (code == BaseException.PARAMERR.getErrorCode()) return 400;
		if (code == BaseException.AUTHFAIL.getErrorCode()) return 403;
		return 500;
	}
	public static String GetResourceRootPath(){
		String path = ClassLoader.getSystemResource("").getPath();

		if (Utils.IsLinux()) return path;

		if (path.charAt(0) == '/' || path.charAt(0) == '\\'){
			path = path.substring(1);
		}

		return path;
	}
	public static Annotation GetAnnotation(Field field, String name){
		Annotation[] arr = field.getAnnotations();
		for (int i = 0; i < arr.length; i++) {
			Annotation item = arr[i];
			Class<?> clazz = item.annotationType();
			if (name.equals(clazz.getSimpleName())) return item;
		}
		return null;
	}
    public static String GetAnnotationValue(Annotation item, String name){
		Class<?> clazz = item.annotationType();
		Method[] methods = clazz.getMethods();
		try{
			for (int i = 0; i < methods.length; i++) {
				Method method = methods[i];
				if (name.equals(method.getName())) return method.invoke(item).toString();
			}
		}
		catch(Exception e){
		}
		return null;
	}
	public static String GetDocString(Class clazz) throws IllegalAccessException{
		class FieldItem{
			public String type = "";
			public String name = "";
			public String remark = "";
			public String extdata = "required";

			FieldItem(Field field){
				name = field.getName();
				type = field.getType().getSimpleName().toLowerCase();

				if (true){
					Annotation required = GetAnnotation(field, "Required");
					Annotation optional = GetAnnotation(field, "Optional");

					if (required != null) remark = GetAnnotationValue(required, "value");
					if (optional != null) remark = GetAnnotationValue(optional, "value");
					if (required == null && optional != null) extdata = "optional";
				}
				else{
					Required required = field.getAnnotation(Required.class);
					Optional optional = field.getAnnotation(Optional.class);

					if (required != null) remark = required.value();
					if (optional != null) remark = optional.value();
					if (required == null && optional != null) extdata = "optional";
				}
			}
			public boolean isObject(){
				switch (type){
					case "int": return false;
					case "byte": return false;
					case "long": return false;
					case "short": return false;
					case "float": return false;
					case "double": return false;
					case "string": return false;
					case "integer": type = "int"; return false;
					case "boolean": type = "bool"; return false;
					default: return true;
				}
			}
		}

		String doc = "";
		Field[] vec = clazz.getFields();

		for (Field field : vec){
			FieldItem item = new FieldItem(field);

			if (item.isObject()){
				Type type = field.getGenericType();
				if (type instanceof ParameterizedType) {
					Class subclazz = (Class)((ParameterizedType)(type)).getActualTypeArguments()[0];

					doc += String.format("\n<tr><td><span>%s</span></td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, "array", item.extdata, item.remark);

					switch (subclazz.getSimpleName().toLowerCase()){
						case "int": break;
						case "byte": break;
						case "long": break;
						case "short": break;
						case "float": break;
						case "double": break;
						case "string": break;
						case "integer": break;
						case "boolean": break;
						default:
							doc += GetDocString(subclazz).replace("\n<tr><td>", "\n<tr><td>&nbsp;&nbsp;<span>&gt;</span>");
							break;
					}
				}
				else{
					doc += String.format("\n<tr><td><span>%s</span></td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, "object", item.extdata, item.remark);
					doc += GetDocString(field.getType()).replace("\n<tr><td>", "\n<tr><td>&nbsp;&nbsp;<span>&gt;</span>");
				}
			}
			else{
				doc += String.format("\n<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, item.type, item.extdata, item.remark);
			}
		}

		while (true){
			String tmp = doc.replace("<span>&gt;</span>&nbsp;&nbsp;", "<span>&nbsp;</span>&nbsp;&nbsp;");

			if (tmp.length() == doc.length()) return doc;

			doc = tmp;
		}
	}
	public static void Process(String path) throws IOException, FileNotFoundException{
		Utils.LoadConfig(path = Utils.Translate(path));

		String apphome = Utils.GetConfig("app.path");

		if (Utils.IsEmpty(apphome)){
			apphome = Utils.GetCurrentPath();
		}

		System.load(Utils.Translate(GetPluginPath() + "/InitSystem.so"));

		AddClassPath(apphome + "/jar");

		WebApp.SetClassPath(GetClassPath());
		WebApp.SetLogFlag(2);
		WebApp.Loop(path);
	}
	public static void AddClassPath(String path){
		String classpath = System.getProperty("java.class.path");

		if (Utils.IsLinux()){
			classpath += ":" + path;
		}
		else{
			classpath += ";" + path;
		}

		System.setProperty("java.class.path", classpath);
	}

	public static String GetConfig(String key){
		return GetConfig(null, key);
	}
	public static String GetRemoteResult(String path){
		return GetRemoteResult(path, null, null, null);
	}
	public static String GetRemoteResult(String path, String param){
		return GetRemoteResult(path, param, null, null);
	}
	public static String GetRemoteResult(String path, String param, String contype){
		return GetRemoteResult(path, param, contype, null);
	}

	public static String CheckConfig() throws Exception{
		String prodpath = null;
		String installpath = null;

		if (Utils.IsLinux()){
			installpath = "/opt/cppweb";
			prodpath = installpath + "/product";
			
		}
		else{
			installpath = "c:/cppweb";
			prodpath = installpath + "/product/win";
		}

		String cfgpath = null;
		String binpath = prodpath + "/bin/";
		String dllpath = prodpath + "/dll/";
		List<File> vec = Utils.GetFolderContent(".");
		List<String> libpaths = new ArrayList<String>();

		System.load(dllpath + "/libjni.stdx.so");

		for (File file : vec){
			String name = file.getName();
			if (name.equals("config.yml")){
				String path = file.getCanonicalPath();
				if (Utils.IsEmpty(cfgpath) || path.length() < cfgpath.length()){
					cfgpath = path;
				}
				break;
			}
		}

		libpaths.add(binpath);
		libpaths.add(dllpath);
		libpaths.add(prodpath + "/lib/mysql/lib");
		libpaths.add(prodpath + "/lib/python/lib");
		libpaths.add(prodpath + "/lib/openssl/lib");
		libpaths.add(prodpath + "/lib/postgresql/lib");

		String tag = Utils.IsLinux() ? ":" : ";";
		String key = Utils.IsLinux() ? "LD_LIBRARY_PATH" : "PATH";

		Utils.SetEnv("CPPWEB_PRODUCT_HOME", prodpath);
		Utils.SetEnv("CPPWEB_INSTALL_HOME", installpath);
		Utils.SetEnv(key, Utils.Join(libpaths, tag) + tag + Utils.GetEnv(key));

		return cfgpath;
	}

	public static void main(String[] args) throws Exception{
		String cfgpath = CheckConfig();

		if (args.length < 1){
			args = new String[1];
			args[0] = cfgpath;
		}

		Process(args[0]);
	}
}