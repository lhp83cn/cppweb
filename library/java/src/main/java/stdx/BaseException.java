package stdx;

import java.util.HashMap;
import java.util.Map;

public class BaseException extends java.lang.RuntimeException{
    int errcode;
    String message;
    Map<String, Object> fields;

    BaseException(int code, String message){
        this.errcode = code;
        this.message = message;
    }
    BaseException(BaseException base, String message){
        this.errcode = base.errcode;
        this.message = Utils.IsEmpty(message) ? base.message : message;
    }

    static final String SYSTEM_ERROR_MSG = "system error";
    public static final BaseException OK = new BaseException(1, "success");
    public static final BaseException FAIL = new BaseException(0, "process failed");
    public static final BaseException ERROR = new BaseException(-1, SYSTEM_ERROR_MSG);
    public static final BaseException IOERR = new BaseException(-2, SYSTEM_ERROR_MSG);
    public static final BaseException SYSERR = new BaseException(-3, SYSTEM_ERROR_MSG);
    public static final BaseException NETERR = new BaseException(-4, SYSTEM_ERROR_MSG);
    public static final BaseException TIMEOUT = new BaseException(-5, "resource timeout");
    public static final BaseException DATAERR = new BaseException(-6, "invalid data");
    public static final BaseException SYSBUSY = new BaseException(-7, "system busy");
    public static final BaseException PARAMERR = new BaseException(-8, "parameter error");
    public static final BaseException NOTFOUND = new BaseException(-9, "resource not found");
    public static final BaseException NETCLOSE = new BaseException(-10, SYSTEM_ERROR_MSG);
    public static final BaseException NETDELAY = new BaseException(-11, SYSTEM_ERROR_MSG);
    public static final BaseException SENDFAIL = new BaseException(-12, SYSTEM_ERROR_MSG);
    public static final BaseException RECVFAIL = new BaseException(-13, SYSTEM_ERROR_MSG);
    public static final BaseException AUTHFAIL = new BaseException(-14, "permission denied");
    public static final BaseException DAYLIMIT = new BaseException(-15, "frequent operation");
    public static final BaseException DUPLICATE = new BaseException(-16, "resource duplicate");
    public static final BaseException UNINSTALL = new BaseException(-17, SYSTEM_ERROR_MSG);
	public static final BaseException NOTCHANGED = new BaseException(-18, "resource not changed");
    public static final BaseException DETACHCONN = new BaseException(-999999, SYSTEM_ERROR_MSG);

    public int getErrorCode(){
        return errcode;
    }
    public String getMessage(){
        return message;
    }
	public Map<String, Object> getFields(){
		return fields;
	}
    public BaseException copy(String msg){
		BaseException res = new BaseException(this, msg);
		res.fields = new HashMap<>();
		return res;
    }
	public BaseException extra(String name, Object value){
		fields.put(name, value);
		return this;
	}
}