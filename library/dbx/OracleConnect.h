#ifndef XG_ORACLECONNECT_H
#define XG_ORACLECONNECT_H
//////////////////////////////////////////////////////////////
#include <oci.h>
#include "DBConnect.h"

class OracleColumnData
{
public:
	ub2 size;
	ub2 type;
	ub2 indicator;
	OCIDefine* handle;
};

class OracleRowData : public RowData
{
	friend class OracleQueryResult;

protected:
	ub1* m_pBuffer;
	int m_iCurIndex;
	int m_iColumCount;
	OracleColumnData* m_pOracleColumnData;

protected
	int getOffset(int index);

public:
	~OracleRowData();

public:
	bool isNull();
	string getString(int index);
	int getDataLength(int index);
	DateTime getDateTime(int index);
	int getData(int index, char* data, int len);
	bool getDateTime(DateTime& datetime, int index);
	OracleRowData(OracleColumnData* data, ub1* buffer, int cols);
};

class OracleQueryResult : public QueryResult
{
	friend class OracleConnect;

protected:
	OCIStmt* m_stmt;
	OCIError* m_pErr;
	int m_iColumCount;
	int m_iBufferSize;
	bool m_bFetchFirst;
	u_char* m_pSharedBuffer;
	OracleColumnData* m_pOracleColumnData;

public:
	~OracleQueryResult();
	
public:
	int rows();
	int cols();
	void close();
	sp<RowData> next();
	bool seek(int ofs);
	int getErrorCode();
	string getErrorString();
	string getColumnName(int index);
	bool getColumnData(ColumnData& data, int index);
	OracleQueryResult(OCIStmt* stmt, OCIError* err, OracleColumnData* data, int iColunCount, u_char* buffer, int iBufferSize);
};

class OracleConnect : public DBConnect
{
	friend class OracleQueryResult;

protected:
	OCIError* m_pErr;
	OCIServer* m_pSrv;  
	OCISvcCtx* m_pSvc;

	static OCIEnv* s_pEnv;  

	static bool FreeHandle(dvoid* handle, int type);
	static bool AllocHandle(dvoid** handle, int type);

	int bind(void* stmt, const vector<DBData*>& vec);

public:
	OracleConnect();
	~OracleConnect();

	void close();
	bool commit();
	bool rollback();
	const char* getSystemName();
	bool begin(bool commited = true);
	int getTables(vector<string>& vec);
	bool setCharset(const string& charset);
	int getPrimaryKeys(vector<string>& vec, const string& tabname);
	bool connect(const string& host, int port, const string& name, const string& user, const string& password);

	int getErrorCode();
	string getErrorString();
	int execute(const string& sqlcmd);
	sp<QueryResult> query(const string& sqlcmd);
	int execute(const string& sqlcmd, const vector<DBData*>& vec);
	sp<QueryResult> query(const string& sqlcmd, const vector<DBData*>& vec);

public:
	static bool Setup();
};
//////////////////////////////////////////////////////////////
#endif